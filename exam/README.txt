The exercise is:
Implement an adaptive integrator which employs Clenshaw-Curtis variable substitution algorithm. Check whether it is more effective than your ordinary adaptive integrator. 

In the home exercise "adaptive integration" I implemented an adaptive integrator that uses a closed set of nodes and Clenshaw-Curtis variable substitution to handle singularities. 
I will implement an adaptive integrator that uses an open set of nodes, so I can compare the two methods.
