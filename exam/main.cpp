#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>

using namespace std;


// new functions
vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                          double acc, double eps);
vector<double> integrator_open(function<double(double)> & f, double a, double b, double f2, double f3,
                                   double acc, double eps);
double func1(double x);
double func2(double x);
double func3(double x);
double func4(double x);
double weightFunction(double a, double b);
double transformedFunction(double a, double b, double theta, function<double(double)> & f);

double PI = 3.14159265358979323846;
double numberOfEvaluations;
double a_finite_orig;
double b_finite_orig;
double a_inf_orig;
double b_inf_orig;

int main() {
    srand((unsigned)time(NULL));

    double a;
    double b;
    double fa = NAN;
    double fb = NAN;
    double acc = 0.001;
    double eps = 0.001;
    function<double(double)> func;
    function<double(double, function<double(double)> & )> infTrans;

    cout << 0*(-fa) << endl;
    cout << " " << endl;

    cout << "PART A)" << endl;
    cout << "Testing a function without singularities. " << endl;
    func = func1;
    a = 0;
    b = 1;
    cout << "Integrating f(x) = sqrt(x) from " << a <<" to " << b << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with open nodes: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralA1 = integrator_open(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralA1[0] << endl;
    cout << "The error is: " << integralA1[1] << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with closed nodes and Clenshaw-Curtis: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralA2 = integrator(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralA2[0] << endl;
    cout << "The error is: " << integralA2[1] << endl;

    cout << " " << endl;

    cout << "PART B)" << endl;
    cout << "Testing a function with a singularity. " << endl;
    func = func2;
    a = 0;
    b = 1;
    cout << "Integrating f(x) = 1/sqrt(x) from " << a <<" to " << b << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with open nodes: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralB1 = integrator_open(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralB1[0] << endl;
    cout << "The error is: " << integralB1[1] << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with closed nodes and Clenshaw-Curtis: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralB2 = integrator(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralB2[0] << endl;
    cout << "The error is: " << integralB2[1] << endl;

    cout << " " << endl;

    cout << "PART C)" << endl;
    cout << "Testing another function with a singularity. " << endl;
    func = func3;
    a = 0;
    b = 1;
    cout << "Integrating f(x) = ln(x)/sqrt(x) from " << a <<" to " << b << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with open nodes: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralC1 = integrator_open(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralC1[0] << endl;
    cout << "The error is: " << integralC1[1] << endl;
    cout << " " << endl;
    cout << "Using adaptive integration with closed nodes and Clenshaw-Curtis: " << endl;
    numberOfEvaluations = 0;
    vector<double> integralC2 = integrator(func, a, b, fa, fb, acc, eps);
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The integral is: " << integralC2[0] << endl;
    cout << "The error is: " << integralC2[1] << endl;

    return 0;
}


// new functions

double transformedFunction(double theta, function<double(double)> & f){
    // theta will always be in the interval 0 to PI.
//    cout << theta << endl;
    if (theta < 0 || theta > PI+1e-6) cout << "WARNING: theta = " << theta << " not in interval 0 to PI."
                " Integral transformation might not work" << endl;
    assert(theta >= 0 && theta <= PI+1e-6);
    double a = a_finite_orig;
    double b = b_finite_orig;
    double sinTheta;
    double cosTheta;

    if (abs(theta-PI) < 1e-6){ // sin(PI) and cos(PI) has to give exactly 0 and -1
        sinTheta = 0;
        cosTheta = -1;
        return 0;
    } else if(abs(theta) < 1e-6){ //sin(0) and cos(0) has to give exactly 0 and 1
        sinTheta = 0;
        cosTheta = 1;
        return 0;
    } else{
        sinTheta = sin(theta);
        cosTheta = cos(theta);
    }
    return 0.5*(b-a)*sinTheta*f(0.5*(cosTheta+1)*(b-a)+a);
}

vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                  double acc, double eps){
    // this integrator uses closed quadratures. That is the end points are used for evaluation
    // the integrator can handle singularities in end points
    double Q;
    double q;
    numberOfEvaluations += 1;

    if (isnan(fa) && isnan(fb)){ // when intergrator is called from main func, f(a) and f(b) is not known

        // transform the integral, so sigularity i end points can be handled
        a_finite_orig = a;
        b_finite_orig = b;

        a = 0;
        b = PI;

        fa = transformedFunction(a,f);
        fb = transformedFunction(b,f);
//        cout << fa << fb << endl;
    }
    double x2 = a+(b-a)/2;
    double f2 = transformedFunction(x2,f);;
//    cout << "interval " << a << " " << x2 << " " << b << endl;
//    cout << "f of " << fa << " " << f2 << " " << fb << endl;

    Q = (1.0/6.0*fa + 4.0/6.0*f2 + 1.0/6.0*fb)*(b-a);
    q = (1.0/4.0*fa + 2.0/4.0*f2 + 1.0/4.0*fb)*(b-a);

//    cout << "Q and q " << Q << " " << q << endl;

    double error = abs(Q-q);
//    cout << "error " << error << endl;
//    cout << "limit " << acc << "+" << eps << "*"<< abs(Q)<< "=" << acc + eps * abs(Q) << endl;
    if (error > acc + eps * abs(Q)){
//        cout << "splitting intervals" << endl;
        acc = acc/sqrt(2.0);
        vector<double> leftHalf = integrator(f,a,x2,fa,f2,acc,eps);
        vector<double> rightHalf = integrator(f,x2,b,f2,fb,acc,eps);
        Q = leftHalf[0] + rightHalf[0];
        error = leftHalf[1] + rightHalf[1];
//        cout << "Q " << Q << endl;
    }

    vector<double> result(2);
    result[0] = Q;
    result[1] = error;
    return  result;
}

vector<double> integrator_open(function<double(double)> & f, double a, double b, double f2, double f3,
                               double acc, double eps){
    // this integrator uses open quadratures. The chosen points are 1/6, 2/6, 4/6, 5/6
    // the points are chosen, such that they can be reused
    double Q;
    double q;
    numberOfEvaluations += 1;
//    cout << " " << endl;

//    cout << "a and b = " << a << " " << b << endl;

    double x1 = a+1.0/6.0*(b-a);
    double x4 = a+5.0/6.0*(b-a);

    if (isnan(f2) && isnan(f3)){
        double x2 = a+2.0/6.0*(b-a);
        double x3 = a+4.0/6.0*(b-a);
        f2 = f(x2);
        f3 = f(x3);
    }
    double f1 = f(x1);
    double f4 = f(x4);


//    cout << "points: " << x1 << " " << x2 << " " << x3 <<  " " << x4 << endl;
//    cout << "f of points: " << f1 << " " << f2 << " " << f3 << " " << f4 << endl;

    Q = (2.0/6.0*f1 + 1.0/6.0*f2 + 1.0/6.0*f3 + 2.0/6.0*f4)*(b-a); // using trapezium rule
    q = (1.0/4.0*f1 + 1.0/4.0*f2 + 1.0/4.0*f3 + 1.0/4.0*f4)*(b-a); // using rectangle rule

//    cout << "Q and q " << Q << " " << q << endl;

    double error = abs(Q-q);
//    cout << "error " << error << endl;
//    cout << "limit " << acc << "+" << eps << "*"<< abs(Q)<< "=" << acc + eps * abs(Q) << endl;
    if (error > acc + eps * abs(Q)){
//        cout << "splitting intervals" << endl;
        acc = acc/sqrt(2.0);
        double midpoint = a + (b-a)/2.0;
        vector<double> leftHalf = integrator_open(f,a,midpoint,f1,f2,acc,eps);
        vector<double> rightHalf = integrator_open(f,midpoint,b,f3,f4,acc,eps);
        Q = leftHalf[0] + rightHalf[0];
        error = leftHalf[1] + rightHalf[1];
//        cout << "Q " << Q << endl;
    }

    vector<double> result(2);
    result[0] = Q;
    result[1] = error;
    return  result;
}


double func1(double x){
    return sqrt(x);
}

double func2(double x){
    return 1/sqrt(x);
}

double func3(double x){
    return log(x)/sqrt(x);
}

double func4(double x){
    return 4*sqrt(1-pow(1-x,2.0));
}

double weightFunction(double a, double b){
    // the choosen weight function is exp(-x)
    return (-exp(-b)/b) - (-exp(-a)/a);
}
