#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "ode.h"
#include "functions.h"

using namespace std;

void printGslVector(const gsl_vector * a){
    int n = a->size;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        cout << setprecision(3) << gsl_vector_get(a,i) << endl;
    }
    cout << endl;
}

void printGslMatrix(const gsl_matrix * A){
    int n = A->size1;
    int m = A->size2;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << setprecision(3) << gsl_matrix_get(A,i,j) << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void odeFunc(double t, gsl_vector * y, gsl_vector * dydt){
//    double fx = func1(gsl_vector_get(y,0));
//    double fx = gsl_vector_get(y,0);
    double fx = 1;
    gsl_vector_set(dydt,0,fx);
}

void rkstepOneTwo(double t, double h, gsl_vector * y, function<void(double, gsl_vector*, gsl_vector*)> func,
                    gsl_vector * yh, gsl_vector * err){
    int n = y->size;
    gsl_vector * k0 = gsl_vector_alloc(n); // k_0
    gsl_vector * k = gsl_vector_alloc(n); // k_1/2

    // use the midpoint method
    func(t, y, k0); // k_0 = f(x_0, y_0)
    gsl_vector * hk0 = gsl_vector_alloc(n); // prepare to calculate y_0 + 0.5*h*k_0
    gsl_vector * y_plus_hk0 = gsl_vector_alloc(n);
    gsl_vector_memcpy(hk0,k0);
    gsl_vector_scale(hk0,0.5*h);
    gsl_vector_memcpy(y_plus_hk0,y);
    gsl_vector_add(y_plus_hk0,hk0);
    func(t+0.5*h, y_plus_hk0, k); // k_1/2 = f(x_0+1/2*h, y_0+1/2*h*k_0)   ,   k = k_1/2

    gsl_vector_scale(k,h); // transform k into K = h*k

    gsl_vector_memcpy(yh,y); // prepare to add the step på y
    gsl_vector_add(yh,k); // y = y_0 + K

    // calc error
    for (int i = 0; i < n; ++i) {
        double temp = (gsl_vector_get(k0,i) - gsl_vector_get(k,i))*h/2;
        gsl_vector_set(err,i,temp);
    }

}

void driver(double t, // current value of variable
            double b, // end point
            double h, // step size
            gsl_vector * y, // y(t)
            double acc,
            double eps,
            function<void(double, double, gsl_vector *, function<void(double, gsl_vector*, gsl_vector*)>,
                         gsl_vector *, gsl_vector * )> stepper, // step function to be used
            function<void(double, gsl_vector *, gsl_vector *)> func)
{
    int n = y->size;
    double a = t;
//    cout << " 1" << endl;

    gsl_vector * yh = gsl_vector_alloc(n); // prepare a vector to contain the next step
    gsl_vector * err = gsl_vector_alloc(n); // a vector for the error
//    gsl_vector * dydt = gsl_vector_alloc(n);


//    stepper(t,h,y,f,yh,err);
    int numberOfSteps = 0;
    int numberOfReavals = 0;
    bool endpointReached = false;
    while (!endpointReached){
        numberOfSteps +=1;
        numberOfReavals = 0;

        double h_i = h;

        bool stepAccepted = false;
        do{
            numberOfReavals += 1;
//            cout << "Stepsize " << h << endl;
//            cout << "Number of evaluations of the step " << numberOfReavals << endl;
            stepper(t,h,y,func,yh,err);
            double norm_yh = 0;
            double error = 0;

            for (int i = 0; i < n; ++i) {
                double yh_i = gsl_vector_get(yh,i);
                norm_yh += yh_i*yh_i;

                double err_i = gsl_vector_get(err,i);
                error += err_i*err_i;
            }
            norm_yh = sqrt(norm_yh);
            error = sqrt(error);
            double tolerance;

            if (error == 0){
//                cout << "The error is equal to 0" << endl;
                h = b-t;
            } else{
                tolerance = (eps*norm_yh+acc)*sqrt(h_i/(b-a)); // should  it be b-a or b-t? b-a i think
//                cout << "h =" << h << "*" << "(" << tolerance << "/" << error << ")^0.25*0.95" << endl;
                h = h*pow(tolerance/error,0.25)*0.95;
            }

//            cout << "error = " << error << " tolerance = " << tolerance << endl;
            if (error < tolerance)stepAccepted = true;
            if (numberOfReavals > 10){
                stepAccepted = true;
//                cout << "very bad step" << endl;
            }
        }while(!stepAccepted && numberOfReavals < 10);
//        cout << "Number of reevaluations = " << numberOfReavals << endl;

//        t += h;
//        cout << "t+h = " << t << endl;
        gsl_vector_memcpy(y,yh);
//        gsl_vector_free(yh);
        if (t > b) h = b-t;
        t += h;
        if (numberOfSteps >= 100000) endpointReached = true;
        if (b == t) endpointReached = true;


    }

    cout << "Number of steps " << numberOfSteps << endl;
    cout << "Evaluation endpoint t = " << t << endl;
}
