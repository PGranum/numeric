#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include <gsl/gsl_integration.h>
#include "functions.h"
#include "ode.h"

using namespace std;

// new functions
vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                          double acc, double eps, function<double(double, function<double(double)> &)> & infTrans);
double weightFunction(double a, double b);
double transformedFunction(double theta, function<double(double)> & f,
                           function<double(double, function<double(double)> &)> & inftrans);

double leftRightInfTrans(double t, function<double(double)> & f);
double leftInfTrans(double t, function<double(double)> & f);
double rightInfTrans(double t, function<double(double)> & f);
double noInfTrans(double t, function<double(double)> & f);

double PI = 3.14159265358979323846;
double numberOfEvaluations;
double a_finite_orig;
double b_finite_orig;
double a_inf_orig;
double b_inf_orig;

int main() {
    srand((unsigned)time(NULL));

    double a = 0;
    double b = 1;
    double fa = NAN;
    double fb = NAN;
    double acc = 0.001;
    double eps = 0.001;
    function<double(double)> func;
    function<double(double, function<double(double)> & )> infTrans;

    cout << " " << endl;
    cout << "PART A)" << endl;
    cout << " " << endl;

    numberOfEvaluations = 0;
    func = func1;
    infTrans = noInfTrans;
    vector<double> integral1 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "The integral from 0 to 1 of sqrt(x) is: " << integral1[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral1[1] << endl;

    cout << " " << endl;

    numberOfEvaluations = 0;
    func = func2;
    vector<double> integral2 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "The integral from 0 to 1 of 1/sqrt(x) is: " << integral2[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral2[1] << endl;

    cout << " " << endl;

    numberOfEvaluations = 0;
    func = func3;
    vector<double> integral3 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "The integral from 0 to 1 of ln(x)/sqrt(x) is: " << integral3[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral3[1] << endl;

    cout << " " << endl;

    numberOfEvaluations = 0;
    acc = 0.0000001;
    eps = 0.0000001;
    func = func4;
    vector<double> integral4 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "A very high accuracy is used for the following integral:" << endl;
    cout << "The integral from 0 to 1 of 4*sqrt(1-(1-x)^2) is: " << setprecision(10) << integral4[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral4[1] << endl;

    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << " " << endl;

    numberOfEvaluations = 0;
    a = 0;
    b = INFINITY;
//    if (isinf(b)) cout << "b is inf: b = " << b << endl;
    acc = 0.001;
    eps = 0.001;
    func = func5;
    vector<double> integral5 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "The integral from 0 to INF of 1/(x*x+1) is: " << integral5[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral5[1] << endl;
    cout << "The integral should give pi/2 = " << PI/2 << endl;

    gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
    double result, error;
    gsl_function F;
    F.function = &gsl_f1;
    gsl_integration_qagiu (&F, 0, 1e-3, 1e-3, 1000, w, &result, &error);
    cout << "Checking the result using gsl integration: " << result << endl;
    gsl_integration_workspace_free (w);

    cout << " " << endl;

    numberOfEvaluations = 0;
    a = 0;
    b = INFINITY;
    acc = 0.001;
    eps = 0.001;
    func = func6;
    vector<double> integral6 = integrator(func, a, b, fa, fb, acc, eps, infTrans);
    cout << "The integral from 0 to INF of exp(-x*x) is: " << integral6[0] << endl;
    cout << "Number of evaluations used: " << numberOfEvaluations << endl;
    cout << "The error is: " << integral6[1] << endl;
    cout << "The integral should give 1/2*sqrt(pi) = " << 0.5*sqrt(PI) << endl;

    gsl_integration_workspace * w2 = gsl_integration_workspace_alloc (1000);
    double result2, error2;
    gsl_function F2;
    F2.function = &gsl_f2;
    gsl_integration_qagiu (&F2, 0, 1e-3, 1e-3, 1000, w2, &result2, &error2);
    cout << "Checking the result using gsl integration: " << result2 << endl;
    gsl_integration_workspace_free (w2);

    cout << " " << endl;
    cout << "PART C)" << endl;
    cout << " " << endl;

    cout << "Solving the integral of sqrt(x) from 0 to 1 using the ODE driver" << endl;
    double n = 1; // size of system of equations
    a = 0; // integration startpoint
    b = 1; // integration endpoint
    double h = 0.01; // stepsize
    acc = 0.01;
    eps = 0.01;
    gsl_vector * y = gsl_vector_alloc(n);
    gsl_vector_set(y,0,0);
    cout << "Starting conditions is:" << endl;
    cout << "y[0] = " << gsl_vector_get(y,0) << endl;
    function<void(double,gsl_vector *,gsl_vector *)> funcToOde = odeFunc;
    driver(a,b,h,y,acc,eps,rkstepOneTwo,funcToOde);
    cout << "y(" << b<< ") = " << gsl_vector_get(y,0) << endl;

    return 0;
}

double transformedFunction(double theta, function<double(double)> & f,
                           function<double(double, function<double(double)> &)> & inftrans){
    // theta will always be in the interval 0 to PI.
//    cout << theta << endl;
    if (theta < 0 || theta > PI+1e-6) cout << "WARNING: theta = " << theta << " not in interval 0 to PI."
                " Integral transformation might not work" << endl;
    assert(theta >= 0 && theta <= PI+1e-6);
    double a = a_finite_orig;
    double b = b_finite_orig;
    double sinTheta;
    double cosTheta;

    if (abs(theta-PI) < 1e-6){ // sin(PI) and cos(PI) has to give exactly 0 and -1
        sinTheta = 0;
        cosTheta = -1;
        return 0;
    } else if(abs(theta) < 1e-6){ //sin(0) and cos(0) has to give exactly 0 and 1
        sinTheta = 0;
        cosTheta = 1;
        return 0;
    } else{
        sinTheta = sin(theta);
        cosTheta = cos(theta);
    }
//    cout << "test: " << inftrans(0.5*(cosTheta+1)*(b-a)+a,f) << " = " << f(0.5*(cosTheta+1)*(b-a)+a) << endl;
//    cout << "advanced: " << inftrans(0.5*(cosTheta+1)*(b-a)+a,f) << endl;
    return 0.5*(b-a)*sinTheta*inftrans(0.5*(cosTheta+1)*(b-a)+a,f);
}

double leftRightInfTrans(double t, function<double(double)> & f){
    return f(t/(1-t*t))*(1+t*t)/pow(1-t*t,2);
}

double leftInfTrans(double t, function<double(double)> & f){
//    cout << "Using left inf trans" << endl;
    double b = b_inf_orig;
    return f(b+t/(1+t))/pow(1+t,2);
}

double rightInfTrans(double t, function<double(double)> & f){
//    cout << "Using right inf trans" << endl;
    double a = a_inf_orig;
//    cout << f(a+t/(1-t)) << endl;
    return f(a+t/(1-t))/pow(1-t,2);
}

double noInfTrans(double t, function<double(double)> & f){
    return f(t);
}

vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                  double acc, double eps, function<double(double, function<double(double)> & )> & infTrans){
    // this integrator uses closed quadratures. That is the end points are used for evaluation
    // the integrator can handle infinite limits and singularities in end points
    double Q;
    double q;
    numberOfEvaluations += 1;
//    cout << " " << endl;

    if (isnan(fa) && isnan(fb)){ // when intergrator is called from main func, f(a) and f(b) is not known
        // check if one of or both of the integration limits are zero. If true the integral should be transformed
        if (isinf(a) && isinf(b)){
//            infTrans = leftRightInfTrans; cout << "Using left-right inf trans" << endl;
            a = -1;
            b = 1;
        }else if(isinf(a)){
            b_inf_orig = b;
//            infTrans = leftInfTrans; cout << "Using left inf trans" << endl;
            a = -1;
            b = 0;
        }else if(isinf(b)){
            a_inf_orig = a;
//            infTrans = rightInfTrans; cout << "Using right inf trans" << endl;
            a = 0;
            b = 1;
        } // else: infTrans will just be the identity function

        // transform the integral, so sigularity i end points can be handled
        a_finite_orig = a;
        b_finite_orig = b;

        a = 0;
        b = PI;

//        cout << "inftrans of a and b " << infTrans(a,f) << " " << infTrans(b,f) << endl;
        fa = transformedFunction(a,f,infTrans);
        fb = transformedFunction(b,f,infTrans);
//        cout << "fa and fb " << fa << fb << endl;
    }
    double x2 = a+(b-a)/2;
    double f2 = transformedFunction(x2,f,infTrans);;
//    cout << "interval " << a << " " << x2 << " " << b << endl;
//    cout << "f of " << fa << " " << f2 << " " << fb << endl;
//    if (abs(1/(b-a)) < 100) cout <<"interval too small" << endl;
//    assert(abs(1/(b-a)) < 100000);

    Q = (1.0/6.0*fa + 4.0/6.0*f2 + 1.0/6.0*fb)*(b-a);
    q = (1.0/4.0*fa + 2.0/4.0*f2 + 1.0/4.0*fb)*(b-a);

//    cout << "Q and q " << Q << " " << q << endl;

    double error = abs(Q-q);
//    cout << "error " << error << endl;
//    cout << "limit " << acc << "+" << eps << "*"<< abs(Q)<< "=" << acc + eps * abs(Q) << endl;
    if (error > acc + eps * abs(Q)){
//        cout << "splitting intervals" << endl;
        acc = acc/sqrt(2.0);
        vector<double> leftHalf = integrator(f,a,x2,fa,f2,acc,eps,infTrans);
        vector<double> rightHalf = integrator(f,x2,b,f2,fb,acc,eps,infTrans);
        Q = leftHalf[0] + rightHalf[0];
        error = leftHalf[1] + rightHalf[1];
//        cout << "Q " << Q << endl;
    }

    vector<double> result(2);
    result[0] = Q;
//    cout << "Q = " << Q << endl;
    result[1] = error;
    return  result;
}

