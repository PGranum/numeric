using namespace std;

double func1(double x);
double func2(double x);
double func3(double x);
double func4(double x);
double func5(double x);
double func6(double x);
double gsl_f1 (double x, void * params);
double gsl_f2 (double x, void * params);
