using namespace std;

void printGslMatrix(gsl_matrix * A);
void printGslVector(const gsl_vector * a);
void odeFunc(double t, gsl_vector * y, gsl_vector * dydt);
void rkstepOneTwo(double t, double h, gsl_vector * y, function<void(double, gsl_vector*, gsl_vector*)> f,
                  gsl_vector * yh, gsl_vector * err);
void driver(double t, // current value of variable
            double b,
            double h, // step size
            gsl_vector * y, // y(t)
            double acc,
            double eps,
            function<void(double, double, gsl_vector *, function<void(double, gsl_vector*, gsl_vector*)>,
                          gsl_vector *, gsl_vector * )> stepper, // step function to be used
            function<void(double, gsl_vector *, gsl_vector *)> func);
