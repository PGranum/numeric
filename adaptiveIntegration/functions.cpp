#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "functions.h"

using namespace std;

double func1(double x){
    return sqrt(x);
}

double func2(double x){
    return 1/sqrt(x);
}

double func3(double x){
    return log(x)/sqrt(x);
}

double func4(double x){
    return 4*sqrt(1-pow(1-x,2.0));
}

double weightFunction(double a, double b){
    // the choosen weight function is exp(-x)
    return (-exp(-b)/b) - (-exp(-a)/a);
}

double func5(double x){
    return 1/(x*x+1);
}

double func6(double x){
    return exp(-x*x);
}

double gsl_f1 (double x, void * params) {
    return  1/(x*x+1);
}

double gsl_f2 (double x, void * params) {
    return  exp(-x*x);
}
