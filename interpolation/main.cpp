#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>

using namespace std;

// functions
double lspline(double x, vector<double> x_vec,vector<double> y_vec);
double qspline(double x, vector<double> x_vec,vector<double> y_vec);
double cspline(double x, vector<double> x_vec,vector<double> y_vec);
double integrator(double z, vector<double> x_vec,vector<double> y_vec);
double fRand(double fMin, double fMax);

int main() {
    srand((unsigned)time(NULL));

    int n = 5;
    vector<double> v(n),w(n); /* in the stack */
    for (int i = 0; i < v.size(); ++i) {
        v[i] = i;
        w[i] = pow(v[i],2);
    }

    double x_i = fRand(0,4);
    cout << " " << endl;
    cout << "PART A)" << endl;
    cout << "Using lspline to calculate: y(" << x_i << ") = " << lspline(x_i,v,w) << endl;
    cout << "The exact result is y(" << x_i << ") = " << pow(x_i,2) << endl;

    // make data for plot
    for (int i = 0; i < 100; i++){
        double point = ((double)n-1)/100*i;
//        cout << n << endl;
        cerr << point << "\t" << pow(point,2) << "\t" << lspline(point,v,w) << "\t" << qspline(point,v,w)
             << "\t" << cspline(point,v,w) << endl;
    }

    //test integrator
    cout << "Using lspline the integral of x² from 0 to " << x_i
         << " is calculated to be " << integrator(x_i,v,w) << endl;
    cout << "The exact result is " << (1.0/3.0)*pow(x_i,3) << endl;

    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << "Using qspline to calculate y(" << x_i << ") = " << qspline(x_i,v,w) << endl;
    cout << "The exact result is y(" << x_i << ") = " << pow(x_i,2) << endl;

    cout << " " << endl;
    cout << "PART C)" << endl;
    cout << "Using cspline to calculate y(" << x_i << ") = " << cspline(x_i,v,w) << endl;
    cout << "The exact result is y(" << x_i << ") = " << pow(x_i,2) << endl;

    cout << " " << endl;
    cout << "A plot has been made illustrating the implemtation of l-, q- and cspline." << endl;

    return 0;
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

// integrator calculates the integral from 0 to z
double integrator(double z, vector<double> x_vec,vector<double> y_vec){
    double integral;
    for (int i = 0; i < 100; ++i) {
        integral += z/100*lspline(z/100*i,x_vec,y_vec);
    }

    return integral;
}

double lspline(double x, vector<double> x_vec,vector<double> y_vec){
    // binary search algorithm
    int n = x_vec.size();
    int i = floor(1.0/2.0*n);
    int noOfIterations = 0;
    while (!(x >= x_vec[i] && x < x_vec[i+1]) && noOfIterations < 10){
        if(x > x_vec[i]){
            i = floor(i+i/2.0);
        }
        else{
            i = floor(i/2.0);
        }
//        cout << "search alg " << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;
        noOfIterations += 1;
    }

//    // stupid search alg
//    int i = 0;
//    while (x > x_vec[i]){
//        if (x == x_vec[i]) {return y_vec[i];};
//        i++;
//    }
//    i = i - 1;

//    cerr << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;
    double deltax = x_vec[i+1] - x_vec[i];
    double deltay = y_vec[i+1] - y_vec[i];
    double p = deltay/deltax;
    return y_vec[i] + p*(x-x_vec[i]);
}

double  qspline(double x, vector<double> x_vec,vector<double> y_vec){
    int n = x_vec.size();
    vector<double> p(n);
    vector<double> c(n);
    vector<double> deltax(n);
    vector<double> deltay(n);

    for (int i = 0; i < n; i++){
        deltax[i] = x_vec[i+1] - x_vec[i];
        deltay[i] = y_vec[i+1] - y_vec[i];
        p[i] = deltay[i]/deltax[i];
    }

    // calculate the c coefficient using forward and then backward recursion
    c[0] = 0;
    for (int i = 0; i < n-1; i++){
        c[i+1] = 1/deltax[i+1]*(p[i+1]-p[i]-c[i]*deltax[i]);
    }
    c[n-1] = 1/2*c[n-1];
    for (int i = n-2; i >= 0; i--){
        c[i] = 1/deltax[i]*(p[i+1]-p[i]-c[i+1]*deltax[i+1]);
    }

    // find correct interval
    int i = floor(1.0/2.0*n);
    int noOfIterations = 0;
    while (!(x >= x_vec[i] && x < x_vec[i+1]) && noOfIterations < 10){
        if(x > x_vec[i]){
            i = floor(i+i/2.0);
        }
        else{
            i = floor(i/2.0);
        }
//        cout << "search alg " << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;
        noOfIterations += 1;
    }
//    cerr << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;

    return y_vec[i] + p[i]*(x-x_vec[i]) + c[i]*(x-x_vec[i])*(x-x_vec[i+1]);
}

double  cspline(double x, vector<double> x_vec,vector<double> y_vec){
    int n = x_vec.size();
    vector<double> p(n-1);
    vector<double> h(n-1);
    vector<double> b(n);
    vector<double> c(n-1);
    vector<double> d(n-1);
    vector<double> D(n);
    vector<double> Q(n-1);
    vector<double> B(n);

    vector<double> deltax(n);
    vector<double> deltay(n);

    for (int i = 0; i < n; i++){
        deltax[i] = x_vec[i+1] - x_vec[i];
        deltay[i] = y_vec[i+1] - y_vec[i];
        h[i] = deltax[i];
        p[i] = deltay[i]/h[i];
    }

    D[0] = 2.0;
    D[n-1] = 2.0;
    Q[0] = 1;
    B[0] = 3*p[1];
    B[n-1] = 3*p[n-1];

    for (int i = 0; i < n-2; i++){
        D[i+1] = 2*h[i]/h[i+1]+2;
        Q[i+1] = h[i]/h[i+1];
        B[i+1] = 3*(p[i] + p[i]*h[i]/h[i+1]);
    }

    // Gauss jordan-eliminate
    for(int i=1;i<n;i++){
        D[i]-= Q[i-1]/D[i-1];
        B[i]-=B[i-1]/D[i-1];
    }

    b[n-1]=B[n-1]/D[n-1];

    for(int i=n-2;i>=0;i--){
        b[i]=(B[i]-Q[i]*b[i+1])/D[i];
    }

    for(int i=0;i<n-1;i++){
        c[i]=(-2*b[i]-b[i+1]+3*p[i])/h[i];
        d[i]=(b[i]+b[i+1]-2*p[i])/h[i]/h[i];
    }


    // find correct interval
    int i = floor(1.0/2.0*n);
    int noOfIterations = 0;
    while (!(x >= x_vec[i] && x < x_vec[i+1]) && noOfIterations < 10){
        if(x > x_vec[i]){
            i = floor(i+i/2.0);
        }
        else{
            i = floor(i/2.0);
        }
//        cout << "search alg " << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;
        noOfIterations += 1;
    }

//    cerr << x_vec[i] << " < " << x << " < " << x_vec[i+1] << endl;
    return y_vec[i] + b[i]*(x-x_vec[i]) + c[i]*pow((x-x_vec[i]),2) + d[i]*pow((x-x_vec[i]),3);
}
