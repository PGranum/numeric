#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "functions.h"

using namespace std;

double rosenbrock (gsl_vector * x){
    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);
//    double res = pow(1-x1,2)+100*pow(x2-x1*x1,2);
//    cout << "x1,x2,result = " << x1 << " " << x2 << " " << res << endl;
    return pow(1-x1,2)+100*pow(x2-x1*x1,2);
}

void rosenbrock_deriv (gsl_vector * x, gsl_vector * df){
    // rosenbrock function is: pow(1-x,2)+100*pow(y-x*x,2);
    // the derivaties are
    // f_1 = df/dx1 = 2*(x1-1)-400*(x1*x2-x1*x1*x1);
    // f_2 = df/dx2 = 200*(x2-x1*x1);

    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    double df1 = 2*(x1-1)-400*(x1*x2-x1*x1*x1);
    double df2 = 200*(x2-x1*x1);

    gsl_vector_set(df,0,df1);
    gsl_vector_set(df,1,df2);

//    cout << "inside func "; printGslVector(x); printGslVector(df);
}

void rosenbrock_Hes (gsl_vector * x, gsl_matrix * H){
    // rosenbrock function is: pow(1-x,2)+100*pow(y-x*x,2) with x1 = x and x2 = y
    // the derivaties are
    // f_1 = df/dx1 = -2*(x1-1)-400*(x1*x2-x1*x1*x1);
    // f_2 = df/dx2 = 200*(x2-x1*x1);

    // the double derivatives used for the jacobian are
    // df_1/dx1 = 2 - 400*(x2 - 3*x1*x1)
    // df_1/dx2 = -400*x1
    // df_2/dx1 = -200*2*x1
    // df_2/dx2 = 200
    int n = x->size;
    assert(n == 2);

    double H_11; // df_1/dx1
    double H_12; // df_1/dx2
    double H_21; // df_2/dx1
    double H_22; // df_2/dx2

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    H_11 = 2 - 400*(x2 - 3*x1*x1);
    H_12 = -400*x1;
    H_21 = -400*x1;
    H_22 = 200;

    gsl_matrix_set(H,0,0,H_11);
    gsl_matrix_set(H,0,1,H_12);
    gsl_matrix_set(H,1,0,H_21);
    gsl_matrix_set(H,1,1,H_22);

}

double himmelBlau (gsl_vector * x){
    // himmelblau func is (x²+y-11)²+(x+y²-7)²
    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    return pow((x1*x1+x2-11),2) + pow((x1+x2*x2-7),2);
}

void himmelblau_deriv (gsl_vector * x, gsl_vector * df){
    // himmelblau func is (x²+y-11)²+(x+y²-7)²
    // the derivaties are
    // f_1 = df/dx1 = 2*x*2*(x*x+y-11)+2*(x+y*y-7) = 4x(x²+y-11) + 2(x+y²-7)
    // f_2 = df/dx2 = 2*(x*x+y-11)+2*y*2*(x+y*y-7) = 2(x²+y-11) + 4y(x+y²-7)

    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    double df1 = 4*x1*(x1*x1+x2-11) + 2*(x1+x2*x2-7);
    double df2 = 2*(x1*x1+x2-11) + 4*x2*(x1+x2*x2-7);

    gsl_vector_set(df,0,df1);
    gsl_vector_set(df,1,df2);

}

void himmelblau_Hes (gsl_vector * x, gsl_matrix * H){
    // himmelblau func is (x²+y-11)²+(x+y²-7)²
    // the derivaties are
    // f_1 = df/dx1 = 2*x*2*(x*x+y-11)+2*(x+y*y-7) = 4x(x²+y-11) + 2(x+y²-7)
    // f_2 = df/dx2 = 2*(x*x+y-11)+2*y*2*(x+y*y-7) = 2(x²+y-11) + 4y(x+y²-7)

    // the double derivatives used for the jacobian are
    // df_1/dx1 = 12x²+4y-44 + 2
    // df_1/dx2 = 4x + 4y
    // df_2/dx1 = 4x + 4y
    // df_2/dx2 = 2 + 4x+8y²-28

    int n = x->size;
    assert(n == 2);

    double H_11; // df_1/dx1
    double H_12; // df_1/dx2
    double H_21; // df_2/dx1
    double H_22; // df_2/dx2

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    H_11 = 12*x1*x1+4*x2-42;
    H_12 = 4*x1+4*x2;
    H_21 = 4*x1+4*x2;
    H_22 = 2+4*x1+12*x2*x2-28;

    gsl_matrix_set(H,0,0,H_11);
    gsl_matrix_set(H,0,1,H_12);
    gsl_matrix_set(H,1,0,H_21);
    gsl_matrix_set(H,1,1,H_22);
}

double dataFunc(gsl_vector * x){
    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);

    int n = x->size;
    assert(n == 3);

    double x1 = gsl_vector_get(x,0); // A
    double x2 = gsl_vector_get(x,1); // T
    double x3 = gsl_vector_get(x,2); // B

    double result = 0;
    double f_i; // A*exp(-t/T)+B

    for (int i = 0; i < N; ++i) {
        f_i = x1*exp(-t[i]/x2)+x3;
        result += pow(f_i-y[i],2)/(e[i]*e[i]); // Master function to minimize: sum((f(t_i)-y_i)²/sigma²)
    }

    return result;
}

void dataFunc_deriv (gsl_vector * x, gsl_vector * df){
    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);

    int n = x->size;
    assert(n == 3);
    double x1 = gsl_vector_get(x,0); // A
    double x2 = gsl_vector_get(x,1); // T
    double x3 = gsl_vector_get(x,2); // B

    double df1 = 0;
    double df2 = 0;
    double df3 = 0;
    double f_i = 0;

    for (int i = 0; i < N; ++i) {
        f_i = x1*exp(-t[i]/x2)+x3;
        df1 += exp(-t[i]/x2)/(e[i]*e[i]) * 2*(f_i-y[i]);
        df2 += x1*t[i]/x2*exp(-t[i]/x2)/(e[i]*e[i]) * 2*(f_i-y[i]);
        df3 += 1/(e[i]*e[i]) * 2*(f_i-y[i]);
    }

    gsl_vector_set(df,0,df1);
    gsl_vector_set(df,1,df2);
    gsl_vector_set(df,2,df3);
}


