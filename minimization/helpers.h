using namespace std;

void printGslMatrix(const gsl_matrix * A);
void qr_gs_decomp(const gsl_matrix * A, gsl_matrix * R, gsl_matrix * Q);
void printGslVector(const gsl_vector * a);
gsl_vector * getVecFromMat(gsl_matrix *A, int i);
void setVecInMatrix(gsl_matrix * A, int i, gsl_vector * a);
void qr_gs_solve(const gsl_matrix * Q, const gsl_matrix * R, gsl_vector * b);
void qr_gs_inverse(const gsl_matrix * Q, const gsl_matrix * R, gsl_matrix * A_inv);
