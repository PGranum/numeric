#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include <time.h>
#include "helpers.h"
#include "functions.h"

using namespace std;

void newton_mini(function<double(gsl_vector *)> & f ,
                 function<void(gsl_vector *, gsl_vector *)> & df,
                 function<void(gsl_vector *, gsl_matrix *)> & Hes, gsl_vector * x_start, double epsilon);

void newton_quasi(function<double(gsl_vector *)> & f ,
                  function<void(gsl_vector *, gsl_vector *)> & df, gsl_vector * x_start, double epsilon);

int main() {
    clock_t tStart = clock();
    srand((unsigned)time(NULL));

    cout << " " << endl;
    cout << "PART A)" << endl;
    cout << " " << endl;

    int n = 2;
    gsl_vector * x_start = gsl_vector_alloc(n);
    double epsilon;
    function<double(gsl_vector * )> f_func;
    function<void(gsl_vector *, gsl_vector *)> df_func;
    function<void(gsl_vector *, gsl_matrix *)> H_func;

    gsl_vector_set(x_start,0,2);
    gsl_vector_set(x_start,1,2);
    cout << "Finding a minimum of the Rosenbrock function. Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    epsilon = 0.001;
    f_func = rosenbrock;
    df_func = rosenbrock_deriv;
    H_func = rosenbrock_Hes;
    newton_mini(f_func,df_func,H_func,x_start,epsilon);
    cout << "The found minimum is ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,4);
    gsl_vector_set(x_start,1,4);
    cout << "Finding a minimum of the Himmelblau function. Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    epsilon = 1e-6;
    f_func = himmelBlau;
    df_func = himmelblau_deriv;
    H_func = himmelblau_Hes;
    newton_mini(f_func,df_func,H_func,x_start,epsilon);
    cout << "The found minimum is ";
    printGslVector(x_start);

    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << "Using a quasi-Newton with Broydens update, where the user supplies the function and the gradient" << endl;
    cout << " " << endl;

    gsl_vector_set(x_start,0,2);
    gsl_vector_set(x_start,1,2);
    cout << "Finding a minimum of the Rosenbrock function using quasi Newton."
            " Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    epsilon = 0.001;
    f_func = rosenbrock;
    df_func = rosenbrock_deriv;
    newton_quasi(f_func,df_func,x_start,epsilon);
    cout << "The found minimum is ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,4);
    gsl_vector_set(x_start,1,4);
    cout << "Finding a minimum of the Himmelblau function using quasi Newton."
            " Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    epsilon = 0.001;
    f_func = himmelBlau;
    df_func = himmelblau_deriv;
    newton_quasi(f_func,df_func,x_start,epsilon);
    cout << "The found minimum is ";
    printGslVector(x_start);

    cout << "The Newton method from the root-finding exercise part A performs as follows:" << endl;
    cout << "12 iterations to find the minimum (1,1) of the Rosenbrock function starting in (2,2)" << endl;
    cout << "94 iterations to find the minimum (3,2) of the Himmelblau function starting in (4,4)" << endl;

    cout << " " << endl;

    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);
    for (int i = 0; i < N; ++i) {
        cerr << t[i] << "\t" << y[i] << "\t" << e[i] << endl;
    }

    n = 3;
    gsl_vector * x_start2 = gsl_vector_alloc(n);
    gsl_vector_set(x_start2,0,2);
    gsl_vector_set(x_start2,1,2);
    gsl_vector_set(x_start2,2,2);
    cout << "Fitting the data to the function A*exp(-t/T)+B. Start guess is (A,T,B) = (" << gsl_vector_get(x_start2,0)
         << "," << gsl_vector_get(x_start2,1) << "," << gsl_vector_get(x_start2,2)  <<")" << endl;
    epsilon = 0.001;
    f_func = dataFunc;
    df_func = dataFunc_deriv;
    newton_quasi(f_func,df_func,x_start2,epsilon);

    cout << "The parameters of the best fit is found to be ";
    printGslVector(x_start2);
    cout << "Which means that the lifetime of the sample is " << gsl_vector_get(x_start2,1) << endl;
    cout << "A plot of the data and the fit has been made" << endl;

    cerr << " " << endl;
    cerr << " " << endl;
    double A = gsl_vector_get(x_start2,0);
    double T = gsl_vector_get(x_start2,1);
    double B = gsl_vector_get(x_start2,2);
    for (int i = 0; i < 100; ++i) {
        double temp_x = 0.1*i;
        double temp_y = A*exp(-temp_x/T)+B;

        cerr << temp_x << "\t" << temp_y << endl;
    }

    cout << " " << endl;
    cout << "Code runtime: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << " s" << endl;
    return 0;
}

void newton_mini(function<double(gsl_vector *)> & f ,
                 function<void(gsl_vector *, gsl_vector *)> & df,
                 function<void(gsl_vector *, gsl_matrix *)> & Hes, gsl_vector * x_start, double epsilon){
    int n = x_start->size;
    double fx;
    gsl_vector * df_vec = gsl_vector_alloc(n);
    gsl_matrix * H = gsl_matrix_alloc(n,n);
    gsl_matrix * Q_H = gsl_matrix_alloc(n,n);
    gsl_matrix * R_H = gsl_matrix_alloc(n,n);
//    gsl_matrix * H_inv = gsl_matrix_alloc(n,n);

    gsl_vector * deltax = gsl_vector_alloc(n);

    int iterations = 0;
    int maxIter = 1000;
    bool converged = false;

    do{
        iterations += 1;
//        cout << iterations << endl;

        // calculate step deltax
        fx = f(x_start);

//        cout << "f(x) = " << fx << endl;
        df(x_start,df_vec);
//        cout << "x_start, df_vec = ";
//        printGslVector(x_start);
//        printGslVector(df_vec);
        Hes(x_start,H);

        qr_gs_decomp(H,R_H,Q_H);
        for (int i = 0; i < n; ++i) {
            double temp = -gsl_vector_get(df_vec,i);
            gsl_vector_set(deltax,i,temp);
        }
        qr_gs_solve(Q_H,R_H,deltax);

//        qr_gs_inverse(Q_H,R_H,H_inv);
//        for (int i = 0; i < n; ++i) {
//            double temp;
//            for (int j = 0; j < n; ++j) {
//                double H_inv_ij = gsl_matrix_get(H_inv,i,j);
//                double df_j = gsl_vector_get(df_vec,j);
//                temp += -H_inv_ij*df_j;
//            }
//            gsl_vector_set(deltax,i,temp);
//        }

        // find a safe lambda
        gsl_vector * xLambdaDx = gsl_vector_alloc(n);
        double f_xLambdaDx;
        double lambda = 2.0;
        double alpha = 0.0001;
        double dxTdf;
        for (int i = 0; i < n; ++i) {
            dxTdf += gsl_vector_get(deltax,i)*gsl_vector_get(df_vec,i);
        }

        do{
            lambda = lambda/2.0;
//            cout << "lambda = " << lambda << endl;
            for (int i = 0; i < n; ++i) {
                double temp = gsl_vector_get(x_start,i) + lambda*gsl_vector_get(deltax,i);
                gsl_vector_set(xLambdaDx,i,temp);
            }
            f_xLambdaDx = f(xLambdaDx);

        }while(f_xLambdaDx > fx + alpha*lambda*dxTdf && lambda > 1.0/64.0);

//        cout << "f(x+lambda*dx) = " << f_xLambdaDx;
//        cout << " f(x) = " << fx << " lambda " << lambda << endl;


        // update x_start
        gsl_vector_memcpy(x_start,xLambdaDx);
//        printGslVector(x_start);

        // check if converged
        df(x_start,df_vec);
//        printGslVector(df_vec);
        double df_norm = 0;
        for (int i = 0; i < n; ++i) {
            df_norm += pow(gsl_vector_get(df_vec,i),2);
        }
        df_norm = sqrt(df_norm);
        if (df_norm < epsilon) converged = true;
//        cout << df_norm << endl;


    }while(!converged && iterations < maxIter);


    if (iterations == maxIter){
        cout << "Maximum number of iterations (1000) reached " << endl;
    } else{
        cout << "Iterations used: " << iterations << endl;
    }

}

void newton_quasi(function<double(gsl_vector *)> & f ,
                 function<void(gsl_vector *, gsl_vector *)> & df, gsl_vector * x_start, double epsilon){
    int n = x_start->size;

    double fx = f(x_start);
    gsl_vector * fxdx = gsl_vector_alloc(n);
    df(x_start,fxdx);
//    gsl_matrix * Hes = gsl_matrix_alloc(n,n);
    gsl_matrix * Hes_inv = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(Hes_inv);
    int numberOfsteps = 0;

    gsl_vector * s = gsl_vector_alloc(n);
    gsl_vector * deltax = gsl_vector_alloc(n);
    gsl_vector * z = gsl_vector_alloc(n);
    double norm_deltax;
    double norm_dfdx;
    double norm_s;
    double fz;

    do{
        numberOfsteps += 1;
//        cout << "Iteration number " << numberOfsteps << endl;
//        cout << "print fxdx: "; printGslVector(fxdx);
//        cout << "print Hes: "; printGslMatrix(Hes_inv);
        gsl_blas_dgemv(CblasNoTrans,-1.0,Hes_inv,fxdx,0.0,deltax); // delta x = -H_inv*dfdx
//        cout << "print deltax: "; printGslVector(deltax);
        norm_deltax = 0;
        for (int i = 0; i < n; ++i) {
            norm_deltax += pow(gsl_vector_get(deltax,i),2);
        }
        norm_deltax = sqrt(norm_deltax);

        gsl_vector_memcpy(s,deltax);
        gsl_vector_scale(s,2); // s = deltax*2

//        cout << "x = ";        printGslVector(x_start);
        int iter = 0;
        bool updateAccepted = false;
        do{
            iter +=1;
            gsl_vector_scale(s,0.5); // s = s/2
            gsl_vector_memcpy(z,x_start);
            gsl_vector_add(z,s); // z = x+s
            fz = f(z);

            double s_dfdx = 0;
            for (int i = 0; i < n; ++i) {
                s_dfdx += gsl_vector_get(s,i) * gsl_vector_get(fxdx,i);
            }
            if (fz < fx + 0.1*s_dfdx || iter > 32) updateAccepted = true;

            norm_s = 0;
            for (int i = 0; i < n; ++i) {
                norm_s += pow(gsl_vector_get(s,i),2);
            }
            norm_s = sqrt(norm_s);
            if (norm_s < 1e-7){
                gsl_matrix_set_identity(Hes_inv);
                updateAccepted = true;
            }
        }while(!updateAccepted);
//        cout << "iterations for step: " << iter << endl;

        gsl_vector * fxdx_z = gsl_vector_alloc(n);
        df(z,fxdx_z); // fxdx_z = df(z)
//        cout << "z = "; printGslVector(z);
//        cout << "fxdx_z = "; printGslVector(fxdx_z);
        gsl_vector * y = gsl_vector_alloc(n);
        gsl_vector_memcpy(y,fxdx);
        gsl_vector_sub(y,fxdx_z); // y = fxdx - fxdx_z
//        cout << "fxdx_z = "; printGslVector(fxdx_z);

        // double upper = (y - H_inv*s)*s^T
        double upper = 0;
        for (int i = 0; i < n; ++i) {
            double temp1_i = 0; // temp1_i = (y - H_inv*s)_i
            double temp2_i = 0; // temp2_i = (H_inv*s)_i
            for (int j = 0; j < n; ++j) {
                temp2_i += gsl_matrix_get(Hes_inv,i,j) * gsl_vector_get(s,j);
            }
            temp1_i = gsl_vector_get(y,i) - temp2_i;
            double s_i = gsl_vector_get(s,i);
            upper += temp1_i*s_i;
        }

        // double lower = y^T*H_inv*s
        double lower = 0;
        for (int i = 0; i < n; ++i) {
            double y_i = gsl_vector_get(y,i);
            double temp1_i = 0; // temp1_i = (H_inv*s)_i
            for (int j = 0; j < n; ++j) {
                temp1_i = gsl_matrix_get(Hes_inv,i,j) * gsl_vector_get(s,j);
            }
            lower += y_i*temp1_i;
        }

        // H_inv = H_inv + upper/lower*H_inv
        gsl_matrix_scale(Hes_inv,1+upper/lower);

        // update step;
        gsl_vector_memcpy(x_start,z);
        fx = fz;
        gsl_vector_memcpy(fxdx,fxdx_z);

        norm_dfdx = 0;
        for (int i = 0; i < n; ++i) {
            norm_dfdx += gsl_vector_get(fxdx,i)*gsl_vector_get(fxdx,i);
        }
        norm_dfdx = sqrt(norm_dfdx);
//        cout << "x = "; printGslVector(x_start);
//        cout << "norm(dfdx) = " << norm_dfdx << endl;
//        cout << "fxdx = "; printGslVector(fxdx);

    }while (norm_deltax > 1e-7 && norm_dfdx > epsilon && numberOfsteps < 10000); // and something else?
    cout << "Number of steps used: " << numberOfsteps << endl;
}
