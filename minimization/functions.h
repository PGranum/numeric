using namespace std;

double rosenbrock (gsl_vector * x);
void rosenbrock_deriv (gsl_vector * x, gsl_vector * df);
void rosenbrock_Hes (gsl_vector * x, gsl_matrix * H);
double himmelBlau (gsl_vector * x);
void himmelblau_deriv (gsl_vector * x, gsl_vector * df);
void himmelblau_Hes (gsl_vector * x, gsl_matrix * H);

double dataFunc(gsl_vector * x);
void dataFunc_deriv (gsl_vector * x, gsl_vector * df);
