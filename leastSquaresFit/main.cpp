#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>

using namespace std;


// functions
void printGslMatrix(const gsl_matrix * A);
void qr_gs_decomp(const gsl_matrix * A, gsl_matrix * R, gsl_matrix * Q);
gsl_vector * getVecFromMat(gsl_matrix *A, int i);
void setVecInMatrix(gsl_matrix * A, int i, gsl_vector * a);
void qr_gs_solve(const gsl_matrix * Q, const gsl_matrix * R, gsl_vector * b);
void qr_gs_inverse(const gsl_matrix * Q, const gsl_matrix * R, gsl_matrix * A_inv);
double identityFunction(double x);
double inverseFunction(double x);
double oneFunction(double x);
void qr_gs_lsFit(gsl_vector * x, gsl_vector * y, gsl_vector * dy, vector<function<double(double)>> & funcs);


int main() {
    srand((unsigned)time(NULL));

    // set up data
    int n = 10; // length of data vector
    gsl_vector * x = gsl_vector_alloc(n);
    gsl_vector * y = gsl_vector_alloc(n);
    gsl_vector * dy = gsl_vector_alloc(n);
    double x_data[]  =  {0.100, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372, 1.995, 2.900};
    double y_data[]  =  {12.644, 9.235, 7.377, 6.460, 5.555, 5.896, 5.673, 6.964, 8.896, 11.355};
    double dy_data[] =  {0.858, 0.359, 0.505, 0.403, 0.683, 0.605, 0.856, 0.351, 1.083, 1.002};
    for (int i = 0; i < n; ++i) {
        gsl_vector_set(y,i,y_data[i]);
        gsl_vector_set(x,i,x_data[i]);
        gsl_vector_set(dy,i,dy_data[i]);
    }

    // fit linear combination of functions to data
    vector<function<double(double)>> functions = {inverseFunction, oneFunction, identityFunction};
    qr_gs_lsFit(x,y,dy,functions);

    cout << "A plot, which illustrates that part A and B are solved, has been created." << endl;

    return 0;
}

void printGslMatrix(const gsl_matrix * A){
    int n = A->size1;
    int m = A->size2;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << setprecision(3) << gsl_matrix_get(A,i,j) << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void printGslVector(const gsl_vector * a){
    int n = a->size;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        cout << setprecision(3) << gsl_vector_get(a,i) << endl;
    }
    cout << endl;
}

void qr_gs_decomp(const gsl_matrix * A, gsl_matrix * R, gsl_matrix * Q){
    if (!(A->size1 >= A->size2)) cout << "qr_gs_decomp need tall matrix as input" << endl;
    assert(A->size1 >= A->size2);
    int n = A->size1;
    int m = A->size2;
    double ipHolder;
    gsl_matrix_memcpy(Q,A);

    for (int i = 0; i < m; ++i) {
        gsl_vector * a_i = getVecFromMat(Q,i);
        gsl_vector * q_i = getVecFromMat(Q,i);

        gsl_blas_ddot(a_i,a_i,&ipHolder);

        gsl_matrix_set(R,i,i,sqrt(ipHolder));

        gsl_vector_memcpy(q_i,a_i);
        gsl_vector_scale(q_i,1.0/gsl_matrix_get(R,i,i));

        setVecInMatrix(Q,i,q_i);

        for (int j = i + 1; j < m; ++j) {
            gsl_vector * a_j = getVecFromMat(Q,j);
            gsl_blas_ddot(q_i,a_j,&ipHolder);
            gsl_matrix_set(R,i,j,ipHolder);
            gsl_vector * temp = gsl_vector_alloc(n);
            gsl_vector_memcpy(temp,q_i);
            gsl_vector_scale(temp,gsl_matrix_get(R,i,j));
            gsl_vector_sub(a_j,temp);

            setVecInMatrix(Q,j,a_j);
        }
    }

    // check that R is upper triangular
//    cout << "Testing that R is upper triangular: ";
    bool testPassed = true;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < i; ++j) {
            if (gsl_matrix_get(R,i,j) != 0){
                cout << "test failed" << endl;
                testPassed = false;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;

    // test that Q^T*Q = 1
//    cout << "Testing that Q^T*Q = 1: ";
    testPassed = true;
    gsl_matrix * QtQ = gsl_matrix_alloc(m,m);
    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,Q,Q,0.0,QtQ);
    for (int i = 0; i < m; ++i) {
        if (!testPassed) break;

        for (int j = 0; j < m; ++j) {
            if (i == j && abs(gsl_matrix_get(QtQ,i,j) - 1.0) > 1e-6){
                cout << "test failed" << endl;
                testPassed = false;
                break;
            }
            else if (i != j && abs(gsl_matrix_get(QtQ,i,j)) > 1e-6){
                cout << "test failed " << endl;
                testPassed = false;
                break;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;

    // test that A = QR
//    cout << "Testing that A = QR: ";
    testPassed = true;
    gsl_matrix * QR = gsl_matrix_alloc(n,m);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Q,R,0.0,QR);
    for (int i = 0; i < n; ++i) {
        if (!testPassed) break;

        for (int j = 0; j < m; ++j) {
            double difference = gsl_matrix_get(QR,i,j) - gsl_matrix_get(A,i,j);
            if (abs(difference) > 1e-6){
                testPassed = false;
                break;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;
}

gsl_vector * getVecFromMat(gsl_matrix *A, int i){
    int n = A->size1;
    gsl_vector * a_i = gsl_vector_alloc(n);
    for (int j = 0; j < n; ++j) {
        gsl_vector_set(a_i,j,gsl_matrix_get(A,j,i));
    }
    return a_i;
}

void setVecInMatrix(gsl_matrix * A, int i, gsl_vector * a){
    int n = A->size1;
    for (int j = 0; j < n; ++j) {
        gsl_matrix_set(A,j,i,gsl_vector_get(a,j));
    }
}

void qr_gs_solve(const gsl_matrix * Q, const gsl_matrix * R, gsl_vector * b){
    if (!(Q->size1 == Q->size2)) cout << "qr_gs_solve only solves square systems" << endl;
    assert(Q->size1 == Q->size2);
    int n = b->size; // requires n = m
    gsl_vector * test = gsl_vector_alloc(n);
//    printGslVector(b);
//    printGslMatrix(Q);
    gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,test); // virker ikke med b i stedet for test???
//    printGslVector(test);

    double x_i;

    for (int i = n-1; i >= 0; i += -1) {
//        cout << "i = " << i << endl;
        gsl_vector_set(b,i,gsl_vector_get(test,i)); // burde ikke være nødvendig

        x_i = gsl_vector_get(b,i);
        for (int j = i+1; j < n; ++j) {
//            cout << "j = " << j << endl;
            x_i += - gsl_vector_get(b,j)*gsl_matrix_get(R,i,j);
        }
        x_i = x_i/gsl_matrix_get(R,i,i);

        gsl_vector_set(b,i,x_i);

        x_i = 0; // reset x_i;
    }
//    printGslVector(b);
}

void qr_gs_inverse(const gsl_matrix * Q, const gsl_matrix * R, gsl_matrix * A_inv){
    // if A = QR, then A^-1 is stored in A_inv
    if (!(A_inv->size1 == A_inv->size2)) cout << "qr_gs_inverse only takes square matrix" << endl;
    assert(A_inv->size1 == A_inv->size2);
    int n = A_inv->size1; // A_inv has to be square matrix
    for (int i = 0; i < n; ++i) {
        gsl_vector * e_i = gsl_vector_alloc(n);
        gsl_vector_set_basis(e_i,i);

        qr_gs_solve(Q,R,e_i);

        setVecInMatrix(A_inv,i,e_i);
        gsl_vector_free(e_i);
    }
}

void qr_gs_lsFit(gsl_vector * x, gsl_vector * y, gsl_vector * dy, vector<function<double(double)>> & funcs){
    int n = y->size;
    int m = funcs.size();

    // write x, y and dy to data
    for (int i = 0; i < n; ++i) {
        cerr << gsl_vector_get(x,i) << "\t" << gsl_vector_get(y,i) << "\t" << gsl_vector_get(dy,i) << endl;
    }
    cerr << "" << endl;
    cerr << "" << endl;

    gsl_matrix * A = gsl_matrix_alloc(n,m);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            gsl_matrix_set(A,i,j,funcs[j](gsl_vector_get(x,i)));
        }
    }

    // calculate coefficient to best fit
    // coefficient solve system R*c = Q^T*b
    gsl_matrix * Q = gsl_matrix_alloc(n,m);
    gsl_matrix * R = gsl_matrix_alloc(m,m);

    qr_gs_decomp(A,R,Q);

    gsl_vector * QTb = gsl_vector_alloc(m);
    gsl_blas_dgemv(CblasTrans,1.0,Q,y,0.0,QTb);

    gsl_vector * c = gsl_vector_alloc(m); // coefficient vector
    double x_i;

    for (int i = m-1; i >= 0; i += -1) {
        // R*c = Q^T*b is solved by backsubstitution
        x_i = gsl_vector_get(QTb,i);
        for (int j = i+1; j < m; ++j) {
            x_i += - gsl_vector_get(c,j)*gsl_matrix_get(R,i,j);
        }
        x_i = x_i/gsl_matrix_get(R,i,i);

        gsl_vector_set(c,i,x_i);

        x_i = 0; // reset x_i;
    }

    //calculate covariance matrix: Sigma = (R^T*R)^-1
    gsl_matrix * Sigma = gsl_matrix_alloc(m,m);
    gsl_matrix * RTR = gsl_matrix_alloc(m,m);
    gsl_matrix * Q_RTR = gsl_matrix_alloc(m,m);
    gsl_matrix * R_RTR = gsl_matrix_alloc(m,m);
    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,R,R,0.0,RTR);
    qr_gs_decomp(RTR,R_RTR,Q_RTR);
    qr_gs_inverse(Q_RTR,R_RTR,Sigma);

    // write plot-data to file
    gsl_vector * xmany = gsl_vector_alloc(100);
    gsl_vector * ymany = gsl_vector_alloc(100);
    gsl_vector * yUpLimMany = gsl_vector_alloc(100);
    gsl_vector * yLowLimMany = gsl_vector_alloc(100);
    double xmin = gsl_vector_min(x);
    double xmax = gsl_vector_max(x);
    double y_temp;
    double x_temp;
    double yUp_temp;
    double yLow_temp;
    double delta_y;

    for (int i = 0; i < 100; ++i) {
        // calculate the x value
        x_temp = xmin + xmax/100*i;
        gsl_vector_set(xmany,i,x_temp);

        // calculate the y value and confidence interval
        for (int j = 0; j < m; ++j) {
            y_temp += gsl_vector_get(c,j)*funcs[j](x_temp);
            delta_y = gsl_matrix_get(Sigma,j,j);
            yLow_temp += (gsl_vector_get(c,j)-delta_y)*funcs[j](x_temp);
            yUp_temp += (gsl_vector_get(c,j)+delta_y)*funcs[j](x_temp);
        }
        gsl_vector_set(ymany,i,y_temp);
        gsl_vector_set(yLowLimMany,i,yLow_temp);
        gsl_vector_set(yUpLimMany,i,yUp_temp);

        // write result to file
        cerr << x_temp << "\t" << y_temp << "\t" << yLow_temp << "\t" << yUp_temp << endl;

        // clear temporary variables
        y_temp = 0;
        yLow_temp = 0;
        yUp_temp = 0;
    }
    cerr << "" << endl;
    cerr << "" << endl;

    cout << "PART A) "<< endl;
    cout << "The data are fitted with the function c1*x^-1 + c2 + c3*x. " << "The coefficients c_i are:" << endl;
    printGslVector(c);
}

double identityFunction(double x){
    return x;
}

double inverseFunction(double x){
    return 1.0/x;
}

double oneFunction(double x){
    return 1.0;
}




