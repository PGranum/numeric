#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "helpers.h"

using namespace std;

double f3(int i, gsl_vector * x){
    // find minimum af funktion f = x²+y², svarer til at finde rødder for f = 2x+y² og f = x²+2y
    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    if (i == 0){
        return 2*x1+pow(x2,2);
    }
    else if (i == 1){
        return pow(x1,2) + 2*x2;
    }
    else{
        return NAN;
    }
}

double rosenbrockFunc (int i, gsl_vector * x){
    // rosenbrock function is: pow(1-x,2)+100*pow(y-x*x,2);
    // the derivaties are
    // f_1 = df/dx1 = 2*(x1-1)-400*(x1*x2-x1*x1*x1);
    // f_2 = df/dx2 = 200*(x2-x1*x1);

    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    if (i == 0){
        return 2*(x1-1)-400*(x1*x2-x1*x1*x1);
    }
    else if (i == 1){
        return 200*(x2-x1*x1);
    }
    else{
        return NAN;
    }
}

void jacobi_rosenbrockFunc (gsl_vector * x, gsl_matrix * J){
    // rosenbrock function is: pow(1-x,2)+100*pow(y-x*x,2) with x1 = x and x2 = y
    // the derivaties are
    // f_1 = df/dx1 = 2*(x1-1)-400*(x1*x2-x1*x1*x1);
    // f_2 = df/dx2 = 200*(x2-x1*x1);

    // the double derivatives used for the jacobian are
    // df_1/dx1 = 2 - 400*(x2 - 3*x1*x1)
    // df_1/dx2 = -400*x1
    // df_2/dx1 = -200*2*x1
    // df_2/dx2 = 200
    int n = x->size;
    assert(n == 2);

    double J_11; // df_1/dx1
    double J_12; // df_1/dx2
    double J_21; // df_2/dx1
    double J_22; // df_2/dx2

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    J_11 = 2 - 400*(x2 - 3*x1*x1);
    J_12 = -400*x1;
    J_21 = -400*x1;
    J_22 = 200;

    gsl_matrix_set(J,0,0,J_11);
    gsl_matrix_set(J,0,1,J_12);
    gsl_matrix_set(J,1,0,J_21);
    gsl_matrix_set(J,1,1,J_22);
}

double himmelblauFunc (int i, gsl_vector * x){
    // himmelblau func is (x²+y-11)²+(x+y²-7)²
    // the derivaties are
    // f_1 = df/dx1 = 2*x*2*(x*x+y-11)+2*(x+y*y-7) = 4x(x²+y-11) + 2(x+y²-7)
    // f_2 = df/dx2 = 2*(x*x+y-11)+2*y*2*(x+y*y-7) = 2(x²+y-11) + 4y(x+y²-7)

    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    if (i == 0){
        return 4*x1*(x1*x1+x2-11) + 2*(x1+x2*x2-7);
    }
    else if (i == 1){
        return 2*(x1*x1+x2-11) + 4*x2*(x1+x2*x2-7);
    }
    else{
        return NAN;
    }
}

void jacobi_himmelblauFunc (gsl_vector * x, gsl_matrix * J){
    // himmelblau func is (x²+y-11)²+(x+y²-7)²
    // the derivaties are
    // f_1 = df/dx1 = 2*x*2*(x*x+y-11)+2*(x+y*y-7) = 4x(x²+y-11) + 2(x+y²-7)
    // f_2 = df/dx2 = 2*(x*x+y-11)+2*y*2*(x+y*y-7) = 2(x²+y-11) + 4y(x+y²-7)

    // the double derivatives used for the jacobian are
    // df_1/dx1 = 12x²+4y-44 + 2
    // df_1/dx2 = 4x + 4y
    // df_2/dx1 = 4x + 4y
    // df_2/dx2 = 2 + 4x+12y²-28

    int n = x->size;
    assert(n == 2);

    double J_11; // df_1/dx1
    double J_12; // df_1/dx2
    double J_21; // df_2/dx1
    double J_22; // df_2/dx2

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    J_11 = 12*x1*x1+4*x2-42;
    J_12 = 4*x1+4*x2;
    J_21 = 4*x1+4*x2;
    J_22 = 2+4*x1+12*x2*x2-28;

    gsl_matrix_set(J,0,0,J_11);
    gsl_matrix_set(J,0,1,J_12);
    gsl_matrix_set(J,1,0,J_21);
    gsl_matrix_set(J,1,1,J_22);
}

double linEqSys (int i, gsl_vector * x){
    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);
    double A = 10000;

    if (i == 0){
        return A*x1*x2-1;
    }
    else if (i == 1){
        return exp(-x1) + exp(-x2) - 1.0 - 1.0/A;
    }
    else{
        return NAN;
    }
}

double myFunc (int i, gsl_vector * x){
    // my func is cos(x)+cos(y)
    int n = x->size;
    assert(n == 2);

    double x1 = gsl_vector_get(x,0);
    double x2 = gsl_vector_get(x,1);

    if (i == 0){
        return -sin(x1);
    }
    else if (i == 1){
        return -sin(x2);
    }
    else{
        return NAN;
    }
}
