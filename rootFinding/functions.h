using namespace std;

double f3(int i, gsl_vector * x);
double linEqSys (int i, gsl_vector * x);
double rosenbrockFunc (int i, gsl_vector * x);
double himmelblauFunc (int i, gsl_vector * x);
double myFunc (int i, gsl_vector * x);
void jacobi_rosenbrockFunc (gsl_vector * x, gsl_matrix * J);
void jacobi_himmelblauFunc (gsl_vector * x, gsl_matrix * J);
