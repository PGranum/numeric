#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "helpers.h"

using namespace std;


double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

double innerProduct(vector<double> x, vector<double> y){
    double result;
    int n = x.size();
    for (int i = 0; i < n; ++i) {
        result += x[i]*y[i];
    }
    return result;
}

gsl_matrix * modifiedGramSchmidt(gsl_matrix * A){
    int n = A->size1;
    int m = A->size2;
    gsl_matrix * Q = gsl_matrix_alloc(n,m);
    gsl_matrix_memcpy(Q,A);
    vector<vector<double>> vectorVector = gslMatrixToVectors(Q);

    for (int i = 0; i < m; ++i) {
        vector<double> q_i = vectorVector[i];
        double norm_qi = sqrt(innerProduct(q_i,q_i));

        // calculate new q_i
        for (int j = 0; j < n; ++j) {
            q_i[j] = q_i[j]/norm_qi;
        }

        for (int j = i+1; j < m; ++j) {
            double ip = innerProduct(q_i,vectorVector[j]);
            for (int k = 0; k < n; ++k) {
                vectorVector[j][k] = vectorVector[j][k] - ip*q_i[k];
            }
        }

        vectorVector[i] = q_i;
    }
    Q = vectorsToGslMatrix(vectorVector);

    return Q;
}

void printGslMatrix(const gsl_matrix * A){
    int n = A->size1;
    int m = A->size2;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << setprecision(3) << gsl_matrix_get(A,i,j) << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void printGslVector(const gsl_vector * a){
    int n = a->size;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        cout << setprecision(5) << gsl_vector_get(a,i) << endl;
    }
    cout << endl;
}

gsl_matrix * vectorsToGslMatrix(vector<vector<double>> vectorVector){
    int m = vectorVector.size();
    int n = vectorVector[0].size();
    gsl_matrix * matrix = gsl_matrix_alloc(n,m);
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            gsl_matrix_set(matrix,j,i,vectorVector[i][j]);
        }
    }
    return matrix;
}

vector<vector<double>> gslMatrixToVectors(gsl_matrix * matrix){
    int n = matrix->size1;
    int m = matrix->size2;
    vector<vector<double>> vectorVector(m);
    vector<double> v_i(n);

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            v_i[j] = gsl_matrix_get(matrix,j,i);
        }
        vectorVector[i] = v_i;
    }
    return vectorVector;
}

vector<double> doubleTimesVec(double number, vector<double> vector){
    int n = vector.size();
    for (int i = 0; i < n; ++i) {
        vector[i] = number*vector[i];
    }
    return vector;
}

void qr_gs_decomp(const gsl_matrix * A, gsl_matrix * R, gsl_matrix * Q){
    if (!(A->size1 >= A->size2)) cout << "qr_gs_decomp need tall matrix as input" << endl;
    assert(A->size1 >= A->size2);
    int n = A->size1;
    int m = A->size2;
    double ipHolder;
    gsl_matrix_memcpy(Q,A);

    for (int i = 0; i < m; ++i) {
        gsl_vector * a_i = getVecFromMat(Q,i);
        gsl_vector * q_i = gsl_vector_alloc(n);//getVecFromMat(Q,i);

        gsl_blas_ddot(a_i,a_i,&ipHolder);

        gsl_matrix_set(R,i,i,sqrt(ipHolder));

        gsl_vector_memcpy(q_i,a_i);
        gsl_vector_scale(q_i,1.0/gsl_matrix_get(R,i,i));

        setVecInMatrix(Q,i,q_i);

        for (int j = i + 1; j < m; ++j) {
            gsl_vector * a_j = getVecFromMat(Q,j);
            gsl_blas_ddot(q_i,a_j,&ipHolder);
            gsl_matrix_set(R,i,j,ipHolder);
            gsl_vector * temp = gsl_vector_alloc(n);
            gsl_vector_memcpy(temp,q_i);
            gsl_vector_scale(temp,gsl_matrix_get(R,i,j));
            gsl_vector_sub(a_j,temp);

            setVecInMatrix(Q,j,a_j);
        }
    }

    // check that R is upper triangular
//    cout << "Testing that R is upper triangular: ";
    bool testPassed = true;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < i; ++j) {
            if (gsl_matrix_get(R,i,j) != 0){
                cout << "test failed. R is not upper triangular" << endl;
                testPassed = false;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;

    // test that Q^T*Q = 1
//    cout << "Testing that Q^T*Q = 1: ";
    testPassed = true;
    gsl_matrix * QtQ = gsl_matrix_alloc(m,m);
    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,Q,Q,0.0,QtQ);
//    printGslMatrix(QtQ);
    for (int i = 0; i < m; ++i) {
        if (!testPassed) break;

        for (int j = 0; j < m; ++j) {
            if (i == j && abs(gsl_matrix_get(QtQ,i,j) - 1.0) > 1e-6){
                cout << "test failed. Q^T*Q not equal to 1 " << endl;
                testPassed = false;
                break;
            }
            else if (i != j && abs(gsl_matrix_get(QtQ,i,j)) > 1e-6){
                cout << "test failed Q^T*Q not equal to 1 " << endl;
                testPassed = false;
                break;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;

    // test that A = QR
//    cout << "Testing that A = QR: ";
    testPassed = true;
    gsl_matrix * QR = gsl_matrix_alloc(n,m);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Q,R,0.0,QR);
    for (int i = 0; i < n; ++i) {
        if (!testPassed) break;

        for (int j = 0; j < m; ++j) {
            double difference = gsl_matrix_get(QR,i,j) - gsl_matrix_get(A,i,j);
            if (abs(difference) > 1e-6){
                testPassed = false;
                break;
            }
        }
    }
//    if (testPassed) cout << "test passed" << endl;
}

gsl_matrix * matrixTimesMatrix(gsl_matrix * A, gsl_matrix * B){
    int nA = A->size1;
    int mA = A->size2;
    int nB = B->size1;
    int mB = B->size2;

    if (mA != nB) cout << "matrix times matrix undefined" << endl;

    vector<vector<double>> BVecVec = gslMatrixToVectors(B);

    gsl_matrix * product = gsl_matrix_alloc(nA,mB);
    for (int i = 0; i < nA; ++i) {
        for (int j = 0; j < mB; ++j) {
            double temp = 0;
            for (int k = 0; k < nB; ++k) {
                temp += gsl_matrix_get(A,i,k)*gsl_matrix_get(B,k,j);
            }
            gsl_matrix_set(product,i,j,temp);
        }
    }

    return product;
}

gsl_matrix * transposeMatrix(gsl_matrix * A){
    int n = A->size2;
    int m = A->size1;
    gsl_matrix * AT = gsl_matrix_alloc(n,m);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            gsl_matrix_set(AT,i,j,gsl_matrix_get(A,j,i));
        }
    }
    return AT;
}

gsl_vector * getVecFromMat(gsl_matrix *A, int i){
    int n = A->size1;
    gsl_vector * a_i = gsl_vector_alloc(n);
    for (int j = 0; j < n; ++j) {
        gsl_vector_set(a_i,j,gsl_matrix_get(A,j,i));
    }
    return a_i;
}

void setVecInMatrix(gsl_matrix * A, int i, gsl_vector * a){
    int n = A->size1;
    for (int j = 0; j < n; ++j) {
        gsl_matrix_set(A,j,i,gsl_vector_get(a,j));
    }
}

void qr_gs_solve(const gsl_matrix * Q, const gsl_matrix * R, gsl_vector * b){
    if (!(Q->size1 == Q->size2)) cout << "qr_gs_solve only solves square systems" << endl;
    assert(Q->size1 == Q->size2);
    int n = b->size; // requires n = m
    gsl_vector * test = gsl_vector_alloc(n);
//    printGslVector(b);
//    printGslMatrix(Q);
    gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,test); // virker ikke med b i stedet for test???
//    printGslVector(test);

    double x_i;

    for (int i = n-1; i >= 0; i += -1) {
//        cout << "i = " << i << endl;
        gsl_vector_set(b,i,gsl_vector_get(test,i)); // burde ikke være nødvendig

        x_i = gsl_vector_get(b,i);
        for (int j = i+1; j < n; ++j) {
//            cout << "j = " << j << endl;
            x_i += - gsl_vector_get(b,j)*gsl_matrix_get(R,i,j);
        }
        x_i = x_i/gsl_matrix_get(R,i,i);

        gsl_vector_set(b,i,x_i);

        x_i = 0; // reset x_i;
    }
//    printGslVector(b);
}

void qr_gs_inverse(const gsl_matrix * Q, const gsl_matrix * R, gsl_matrix * A_inv){
    if (!(A_inv->size1 == A_inv->size2)) cout << "qr_gs_inverse only takes square matrix" << endl;
    assert(A_inv->size1 == A_inv->size2);
    int n = A_inv->size1; // A_inv has to be square matrix
    for (int i = 0; i < n; ++i) {
        gsl_vector * e_i = gsl_vector_alloc(n);
        gsl_vector_set_basis(e_i,i);

        qr_gs_solve(Q,R,e_i);

        setVecInMatrix(A_inv,i,e_i);
        gsl_vector_free(e_i);
    }
}
