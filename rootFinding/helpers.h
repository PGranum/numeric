using namespace std;

double fRand(double fMin, double fMax);
double innerProduct(vector<double> x, vector<double> y);
gsl_matrix * modifiedGramSchmidt(gsl_matrix * A);
void printGslMatrix(const gsl_matrix * A);
gsl_matrix * vectorsToGslMatrix(vector<vector<double>> vectorVector);
vector<vector<double>> gslMatrixToVectors(gsl_matrix * matrix);
vector<double> doubleTimesVec(double number, vector<double> vector);
void qr_gs_decomp(const gsl_matrix * A, gsl_matrix * R, gsl_matrix * Q);
gsl_matrix * matrixTimesMatrix(gsl_matrix * A, gsl_matrix * B);
gsl_matrix * transposeMatrix(gsl_matrix * A);
void printGslVector(const gsl_vector * a);
gsl_vector * getVecFromMat(gsl_matrix *A, int i);
void setVecInMatrix(gsl_matrix * A, int i, gsl_vector * a);
void qr_gs_solve(const gsl_matrix * Q, const gsl_matrix * R, gsl_vector * b);
void qr_gs_inverse(const gsl_matrix * Q, const gsl_matrix * R, gsl_matrix * A_inv);
