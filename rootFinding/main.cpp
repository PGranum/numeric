#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "helpers.h"
#include "functions.h"

using namespace std;

// functions
void newton(function<double(int, gsl_vector *)> & f , gsl_vector * x_start, double dx, double epsilon);
void newton_with_jacobian(function<double(int, gsl_vector *)> & f , function<void(gsl_vector *, gsl_matrix *)> & jacobi,
                          gsl_vector * x_start, double dx, double epsilon);

int main() {
    srand((unsigned)time(NULL));

    cout << " " << endl;
    cout << "PART A)" << endl;

    int n = 2;
    gsl_vector * x_start = gsl_vector_alloc(n);
    double dx;
    double epsilon;
    function<double(int, gsl_vector *)> func;
    function<void(gsl_vector *, gsl_matrix *)> jacob;

    gsl_vector_set(x_start,0,10.0);
    gsl_vector_set(x_start,1,1e-5);
    cout << "Solving the system of equations. Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.01;
    epsilon = 0.001;
    func  = linEqSys;
    newton(func,x_start,dx,epsilon);
    cout << "The found solution to the system of linear equations is ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,2);
    gsl_vector_set(x_start,1,2);
    cout << "Finding minimum of Rosenbrock function. Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.001;
    epsilon = 0.001;
    func  = rosenbrockFunc;
    newton(func,x_start,dx,epsilon);
    cout << "The found mimimum of the Rosenbrock function is ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,1);
    gsl_vector_set(x_start,1,1);
    cout << "Finding minimum of Himmelblau function. Start guess is (x,y) = (" << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.001;
    epsilon = 0.001;
    func  = himmelblauFunc;
    newton(func,x_start,dx,epsilon);
    cout << "The found mimimum of the Himmelblau function is  ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,2);
    gsl_vector_set(x_start,1,2);
    cout << "Finding minimum of f(x,y) = cos(x)+cos(y). Start guess is (x,y) = ("
         << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.001;
    epsilon = 0.001;
    func  = myFunc;
    newton(func,x_start,dx,epsilon);
    cout << "The found minimum of f(x,y) = cos(x)+cos(y) is ";
    printGslVector(x_start);

    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << " " << endl;

    gsl_vector_set(x_start,0,2);
    gsl_vector_set(x_start,1,2);
    cout << "Finding minimum of Rosenbrock function using newton with jacobian. Start guess is (x,y) = ("
         << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.001;
    epsilon = 0.001;
    func  = rosenbrockFunc;
    jacob = jacobi_rosenbrockFunc;
    newton_with_jacobian(func,jacob,x_start,dx,epsilon);
    cout << "The found minimum of the Rosenbrock function using newton with jacobian is ";
    printGslVector(x_start);

    gsl_vector_set(x_start,0,1);
    gsl_vector_set(x_start,1,1);
    cout << "Finding minimum of Himmelblau function using newton with jacobian. Start guess is (x,y) = ("
         << gsl_vector_get(x_start,0)
         << "," << gsl_vector_get(x_start,1) << ")" << endl;
    dx = 0.001;
    epsilon = 0.0001;
    func  = himmelblauFunc;
    jacob = jacobi_himmelblauFunc;
    newton_with_jacobian(func,jacob,x_start,dx,epsilon);
    cout << "The found minimum of the Himmelblau function using newton with jacobian is ";
    printGslVector(x_start);

    cout << " " << endl;
    cout << "PART C)" << endl;
    cout << "Not iplemented" << endl;

    return 0;
}

void newton(function<double(int, gsl_vector *)> & f , gsl_vector * x_start, double dx, double epsilon){
    int n = x_start->size;
    gsl_vector * fx = gsl_vector_alloc(n);
    gsl_vector * xPlusdx = gsl_vector_alloc(n);
    gsl_matrix * J = gsl_matrix_alloc(n,n);
    gsl_matrix * Q_J = gsl_matrix_alloc(n,n);
    gsl_matrix * R_J = gsl_matrix_alloc(n,n);

    bool converged = false;
    int iter = 0;
    int maxIterations = 1000;

    while (!converged && iter < maxIterations){
        iter += 1;
        if (iter == maxIterations) cout << "Maximum number of iterations reached" << endl;
//        cout << "iteration no " << iter << endl;

        // calculates f(x)
        for (int i = 0; i < n; ++i) {
            gsl_vector_set(fx,i,f(i,x_start));
        }

//        printGslVector(x_start);
//        printGslVector(fx);

        // calculate jacobi
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                gsl_vector_memcpy(xPlusdx,x_start);
                double x_j_plus_dx = gsl_vector_get(x_start,j)+dx;
                gsl_vector_set(xPlusdx,j,x_j_plus_dx);
//                cout << "f(x+dx) = " << f(i,xPlusdx) << " f(x) = " << gsl_vector_get(fx,i) << " dx = " << dx << endl;
                double J_ij = (f(i,xPlusdx)-gsl_vector_get(fx,i))/dx;
                gsl_matrix_set(J,i,j,J_ij);
            }
        }
//        printGslMatrix(J);

        // solve J*deltax = -f(x) for delta x
        gsl_vector * deltax = gsl_vector_alloc(n); // creates a vector that will later hold deltax's
        for (int i = 0; i < n; ++i) {
            double temp = gsl_vector_get(fx,i);
            gsl_vector_set(deltax,i,-temp); // first the deltax will hold right hand side of equation
        }

        qr_gs_decomp(J,R_J,Q_J);
//        printGslMatrix(Q_J);
//        printGslMatrix(R_J);
        qr_gs_solve(Q_J,R_J,deltax); // delta x now lies in deltax

        // calculate a safe lambda
        double lambda = 2.0;
        gsl_vector * xPlusLambdaDx = gsl_vector_alloc(n);
        double norm_fxldx;
        double norm_fx;
        for (int i = 0; i < n; ++i) {
            norm_fx += pow(gsl_vector_get(fx,i),2.0);
        }
        norm_fx = sqrt(norm_fx);

        do{
            lambda = lambda/2;
            norm_fxldx = 0;

            for (int i = 0; i < n; ++i) {
                double temp = gsl_vector_get(x_start,i) + lambda*gsl_vector_get(deltax,i);
                gsl_vector_set(xPlusLambdaDx,i,temp);

                norm_fxldx += pow(f(i,xPlusLambdaDx),2.0);
            }
            norm_fxldx = sqrt(norm_fxldx);


        }while(norm_fxldx < (1-lambda/2)*norm_fx && lambda > 1.0/64.0);

        // update x_start
        gsl_vector_memcpy(x_start,xPlusLambdaDx);

        // check if converged
        norm_fx = 0;
        for (int i = 0; i < n; ++i) {
            gsl_vector_set(fx,i,f(i,x_start));
            norm_fx += pow(gsl_vector_get(fx,i),2.0);
        }
        norm_fx = sqrt(norm_fx);
        if (norm_fx < epsilon) converged = true;
    }

    cout << "Iterations used: " << iter << endl;

}

void newton_with_jacobian(function<double(int, gsl_vector *)> & f , function<void(gsl_vector *, gsl_matrix *)> & jacobi,
                          gsl_vector * x_start, double dx, double epsilon){
    int n = x_start->size;
    gsl_vector * fx = gsl_vector_alloc(n);
//    gsl_vector * xPlusdx = gsl_vector_alloc(n);
    gsl_matrix * J = gsl_matrix_alloc(n,n);
    gsl_matrix * Q_J = gsl_matrix_alloc(n,n);
    gsl_matrix * R_J = gsl_matrix_alloc(n,n);

    bool converged = false;
    int iter = 0;
    int maxIterations = 1000;

    while (!converged && iter < maxIterations){
        iter += 1;
        if (iter == maxIterations) cout << "Maximum number of iterations reached" << endl;
//        cout << "iteration no " << iter << endl;

        // calculates f(x)
        for (int i = 0; i < n; ++i) {
            gsl_vector_set(fx,i,f(i,x_start));
        }

        // calculate jacobi
        jacobi(x_start,J);

        // solve J*deltax = -f(x) for delta x
        gsl_vector * deltax = gsl_vector_alloc(n); // creates a vector that will later hold deltax's
        for (int i = 0; i < n; ++i) {
            double temp = gsl_vector_get(fx,i);
            gsl_vector_set(deltax,i,-temp); // first the deltax will hold right hand side of equation
        }

        qr_gs_decomp(J,R_J,Q_J);
        qr_gs_solve(Q_J,R_J,deltax); // delta x now lies in deltax

        double lambda = 2.0;
        gsl_vector * xPlusLambdaDx = gsl_vector_alloc(n);
        double norm_fxldx;
        double norm_fx;
        for (int i = 0; i < n; ++i) {
            norm_fx += pow(gsl_vector_get(fx,i),2.0);
        }
        norm_fx = sqrt(norm_fx);

        do{
            lambda = lambda/2;
            norm_fxldx = 0;

            for (int i = 0; i < n; ++i) {
                double temp = gsl_vector_get(x_start,i) + lambda*gsl_vector_get(deltax,i);
                gsl_vector_set(xPlusLambdaDx,i,temp);

                norm_fxldx += pow(f(i,xPlusLambdaDx),2.0);
            }
            norm_fxldx = sqrt(norm_fxldx);


        }while(norm_fxldx < (1-lambda/2)*norm_fx && lambda > 1.0/64.0);

        // update x_start
        gsl_vector_memcpy(x_start,xPlusLambdaDx);

        // check if converged
        norm_fx = 0;
        for (int i = 0; i < n; ++i) {
            gsl_vector_set(fx,i,f(i,x_start));
            norm_fx += pow(gsl_vector_get(fx,i),2.0);
        }
        norm_fx = sqrt(norm_fx);
//        printGslVector(fx);
        if (norm_fx < epsilon) converged = true;
    }

    cout << "Iterations used: " << iter << endl;
}