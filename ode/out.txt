 
PART A)
 
The system of equations is:
dydt[0] = y[1]
dydt[1] = -y[0]

Solving the system of equations for the cos function
Starting conditions is:
y[0] = 1
y[1] = 0
Number of steps 9281
Evaluation endpoint t = 3.1415
y(3.1415) = 
-1
0.000736


Solving the system of equations for the sin function
Starting conditions is:
y[0] = 0
y[1] = 1
Number of steps 9281
Evaluation endpoint t = 3.14
y(3.14) = 
-0.000736
-1

 
PART B)
 
The path of the most recently solved problem is saved in the matrix "path".
The first 10 rows can be seen below:
t	y[1]	y[2]	
0.00209	0.00218	1	
0.00304	0.00317	1	
0.00369	0.00384	1	
0.00422	0.0044	1	
0.0047	0.0049	1	
0.00516	0.00539	1	
0.00561	0.00585	1	
0.00604	0.00629	1	
0.00645	0.00672	1	
0.00686	0.00714	1	
