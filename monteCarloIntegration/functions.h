using namespace std;

double func1(double x);
double func2(double x);
double func3(double x);
double func4(double x);
double testFunc(gsl_vector * x);
double func5(gsl_vector * v);
double func6(gsl_vector * v);
