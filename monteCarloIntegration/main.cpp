#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "functions.h"
#include "helpers.h"

using namespace std;

// functions
//double fRand(double fMin, double fMax);
//void printGslMatrix(const gsl_matrix * A);
//void printGslVector(const gsl_vector * a);
// new functions
vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                          double acc, double eps);
vector<double> plainMC(function<double(gsl_vector *)> & f, gsl_vector * a, gsl_vector * b, int N);

double numberOfEvaluations;
//double PI = 3.14159265358979323846;


int main() {
    srand((unsigned)time(NULL));

    int n = 3; // dimension of the problem
    gsl_vector * a = gsl_vector_alloc(n);
    gsl_vector * b = gsl_vector_alloc(n);
    int N; // number of integration points
    function<double(gsl_vector *)> func;
    vector<double> result;

    cout << " " << endl;
    cout << "PART A)" << endl;
    cout << " " << endl;

    N = 100;
    for (int i = 0; i < n; ++i) {
        gsl_vector_set(a,i,0);
        gsl_vector_set(b,i,1);
    }
    func = func6;
    result = plainMC(func, a, b, N);
    cout << "Integrating the function sqrt(x)sqrt(y)sqrt(z) over the volume 0 < x,y,z < 1" << endl;
    cout << "Number of points used for integration: " << N << endl;
    cout << "The result is: " << result[0] <<  endl;
    cout << "The error is: " << result[1] << endl;

    cout << " " << endl;

    N = 10000000;
    for (int i = 0; i < n; ++i) {
        gsl_vector_set(a,i,0);
        gsl_vector_set(b,i,3.14159265358979323846);
    }
    func = func5;
    result = plainMC(func, a, b, N);
    cout << "Integrating the function (1-cos(x)cos(y)cos(z))^-1*pi^3 over the volume 0 < x,y,z < pi" << endl;
    cout << "Number of points used for integration: " << N << endl;
    cout << "The result is: " << result[0] <<  endl;
    cout << "The error is: " << result[1] << endl;

    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << " " << endl;

    for (int i = 0; i < n; ++i) {
        gsl_vector_set(a,i,0);
        gsl_vector_set(b,i,1);
    }
    func = func6;
    cout << "Testing that the error scales as 1/sqrt(N): " << endl;
    for (int i = 1; i <= 200; ++i) {
        N = i;
        result = plainMC(func, a, b, N);
        cerr << N << "\t" << 1/sqrt(N) << "\t" << 6*result[1] << endl;
    }
    cout << "A plot of error vs N has been created " << endl;


    return 0;
}
//
//double fRand(double fMin, double fMax){
//    double f = (double)rand() / RAND_MAX;
//    return fMin + f * (fMax - fMin);
//}
//
//void printGslMatrix(const gsl_matrix * A){
//    int n = A->size1;
//    int m = A->size2;
//    cout << endl;
//    for (int i = 0; i < n; ++i) {
//        for (int j = 0; j < m; ++j) {
//            cout << setprecision(3) << gsl_matrix_get(A,i,j) << "\t";
//        }
//        cout << endl;
//    }
//    cout << endl;
//}
//
//void printGslVector(const gsl_vector * a){
//    int n = a->size;
//    cout << endl;
//    for (int i = 0; i < n; ++i) {
//        cout << setprecision(3) << gsl_vector_get(a,i) << endl;
//    }
//    cout << endl;
//}


// new functions

vector<double> plainMC(function<double(gsl_vector *)> & f, gsl_vector * a, gsl_vector * b, int N){
    int n = a->size;
    gsl_vector * randomX = gsl_vector_alloc(n);
    double volume = 1;
    for (int i = 0; i < n; ++i) {
        double a_i = gsl_vector_get(a,i);
        double b_i = gsl_vector_get(b,i);
        volume = volume*(b_i-a_i);
    }

    double sum1, sum2, fx;

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < n; ++j) {
            double a_i = gsl_vector_get(a,j);
            double b_i = gsl_vector_get(b,j);
            double rand = fRand(0,1);
            gsl_vector_set(randomX,j,a_i+rand*(b_i-a_i));
        }

        fx = f(randomX);
        sum1 += fx;
        sum2 += fx*fx;
    }

    double mean = sum1/N;
    double sigma = sqrt(sum2/N - mean*mean);
    double SIGMA = sigma/sqrt(N);

    vector<double> result(2);
    result[0] = mean*volume; // integral estimation
    result[1] = SIGMA*volume; // error
    return result;
}

vector<double> integrator(function<double(double)> & f, double a, double b, double fa, double fb,
                          double acc, double eps){
    // this integrator uses closed quadratures. That is the end points are used for evaluation
    double Q;
    double q;
    numberOfEvaluations += 1;
//    cout << " " << endl;

    double x2 = a+(b-a)/2;
    if (isnan(fa) && isnan(fb)){ // when intergrator is called from main func, f(a) and f(b) is not known
        fa = f(a);
        fb = f(b);
    }
    double f2 = f(x2);
//    cout << "interval " << a << " " << x2 << " " << b << endl;
//    cout << "f of " << fa << " " << f2 << " " << fb << endl;
//    if (abs(1/(b-a)) < 100) cout <<"interval too small" << endl;
//    assert(abs(1/(b-a)) < 100000);

    Q = (1.0/6.0*fa + 4.0/6.0*f2 + 1.0/6.0*fb)*(b-a);
    q = (1.0/4.0*fa + 2.0/4.0*f2 + 1.0/4.0*fb)*(b-a);

//    cout << "Q and q " << Q << " " << q << endl;

    double error = abs(Q-q);
//    cout << "error " << error << endl;
//    cout << "limit " << acc << "+" << eps << "*"<< abs(Q)<< "=" << acc + eps * abs(Q) << endl;
    if (error > acc + eps * abs(Q)){
//        cout << "splitting intervals" << endl;
        acc = acc/sqrt(2.0);
        vector<double> leftHalf = integrator(f,a,x2,fa,f2,acc,eps);
        vector<double> rightHalf = integrator(f,x2,b,f2,fb,acc,eps);
        Q = leftHalf[0] + rightHalf[0];
        error = leftHalf[1] + rightHalf[1];
//        cout << "Q " << Q << endl;
    }

    vector<double> result(2);
    result[0] = Q;
    result[1] = error;
    return  result;
}

