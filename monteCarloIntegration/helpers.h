using namespace std;

double fRand(double fMin, double fMax);
void printGslMatrix(const gsl_matrix * A);
void printGslVector(const gsl_vector * a);
