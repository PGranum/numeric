#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_blas.h>
#include <functional>
#include "functions.h"

using namespace std;

double PI = 3.14159265358979323846;

double testFunc(gsl_vector * x){
    int n = x->size;
    double result = 1;
    for (int i = 0; i < n; ++i) {
        double x_i = gsl_vector_get(x,i);
        result = result*sqrt(x_i);
    }
    return result;
}

double func5(gsl_vector * v){
    double x = gsl_vector_get(v,0);
    double y = gsl_vector_get(v,1);
    double z = gsl_vector_get(v,2);

    return 1/((1-cos(x)*cos(y)*cos(z))*PI*PI*PI);
}
double func6(gsl_vector * v){
    double x = gsl_vector_get(v,0);
    double y = gsl_vector_get(v,1);
    double z = gsl_vector_get(v,2);

    return sqrt(x)*sqrt(y)*sqrt(z);
}


double func1(double x){
    return sqrt(x);
}

double func2(double x){
    return 1/sqrt(x);
}

double func3(double x){
    return log(x)/sqrt(x);
}

double func4(double x){
    return 4*sqrt(1-pow(1-x,2.0));
}



