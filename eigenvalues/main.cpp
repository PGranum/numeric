#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <iomanip>
#include <gsl/gsl_blas.h>

using namespace std;

// functions
double fRand(double fMin, double fMax);
void printGslMatrix(gsl_matrix * A);
void printGslVector(const gsl_vector * a);
void jacobiEigvalAlg(gsl_matrix * A, gsl_matrix * V);
gsl_matrix * createJacobiMatrix(int n, int p, int q, double phi);
void jacobiEigByEigAlg(gsl_matrix * A, gsl_matrix * V, int numberOfEigVals);
void jacobiClassical(gsl_matrix * A, gsl_matrix * V);

int main() {
    srand((unsigned)time(NULL));
    cout << " " << endl;
    cout << "PART A)" << endl;

    int n = 5;
    gsl_matrix * A = gsl_matrix_alloc(n,n); // generate a symmetric matrix
    double rand;
    for (int i = 0; i < n; ++i) {
        for (int j = i; j < n; ++j) {
            rand = fRand(0.1,10);
            gsl_matrix_set(A,i,j,rand);
            gsl_matrix_set(A,j,i,rand);
        }
    }

    cout << "Generating a random symmetric matrix A" << endl;
    printGslMatrix(A);

    cout << "Perform eigenvalue decomposition " << endl;
    gsl_matrix * V = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(V);
//    gsl_matrix * D = gsl_matrix_alloc(n,n);
//    gsl_matrix_set_zero(D);

    gsl_matrix * A_copy = gsl_matrix_alloc(n,n);
    gsl_matrix_memcpy(A_copy,A);

    jacobiEigvalAlg(A,V);

    cout << "Print matrix D consisting of eigenvectors for A:" << endl;
    printGslMatrix(A);

//    gsl_matrix * VTV = gsl_matrix_alloc(n,n);
//    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,V,0.0,VTV);
//    cout << "print V^T*V" << endl;
//    printGslMatrix(VTV);
//
//
//    gsl_matrix * VVT = gsl_matrix_alloc(n,n);
//    gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,V,V,0.0,VVT);
//    cout << "print V*V^T" << endl;
//    printGslMatrix(VVT);

    gsl_matrix * VTAV = gsl_matrix_alloc(n,n);
    gsl_matrix * VTAV_temp = gsl_matrix_alloc(n,n);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_copy,V,0.0,VTAV_temp);
    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,VTAV_temp,0.0,VTAV);

    cout << "Check that V^T*A*V = D. Print V^T*A*V" << endl;
    printGslMatrix(VTAV);

////////////////////part 2//////////////
    cout << " " << endl;
    cout << "PART B)" << endl;
    cout << "Calculating the lowest eigenvalue for the same matrix A" << endl;

    gsl_matrix * V2 = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(V2);
    gsl_matrix_memcpy(A,A_copy);
    jacobiEigByEigAlg(A,V2,1);
    cout << "print A after eig by eig diagonalization" << endl;
    printGslMatrix(A);

    cout << "Answers to questions:" << endl;
    cout << "The elements in the first row is not affected by the second run because the elements in the row are"
            "approaching zero, when the update converges on the row. " << endl;
    cout << "Consider the formula used to calculate phi: tan(2*phi) = 2*A_pq/(A_qq-A_pp). When A_qq > A_pp the lower"
            "eigenvalue will be \"rotated up\" in the matrix due to the choice of angle." << endl;
    cout << "By rotating with pi instead of zero the order of the eigenvalues will be reversed." << endl;

    cout << " " << endl;
    cout << "PART C)" << endl;

    cout << "Calculating eigenvalues for the same matrix A using classical Jacobi" << endl;
    gsl_matrix * V3 = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(V3);
    gsl_matrix_memcpy(A,A_copy);
    jacobiClassical(A,V3);
    cout << "print A after eig by eig diagonalization" << endl;
    printGslMatrix(A);

    return 0;
}

void jacobiEigvalAlg(gsl_matrix * A, gsl_matrix * V){
    // man bør kun arbejde med den strnegt øvre del af A og en vector med diagonalelementerne.
    // det er dog ok at gære som nedenstående. så længe der kun er n^2 operationer
    int n = A->size1;
    bool converged = false;

//    gsl_vector * diagonalElements = gsl_vector_alloc(n);
//    for (int i = 0; i < n; ++i) {
//        gsl_vector_set(diagonalElements,i,gsl_matrix_get(A,i,i));
//    }

    double sumOfOffElements;
    double numberOfSweeps;
    double numberOfRotations;

    while (!converged){
        sumOfOffElements = 0;
        numberOfSweeps += 1;

        double A_ij;
        for (int p = 0; p < n; ++p) {
            for (int q = p+1; q < n; ++q) {
                numberOfRotations += 1;

                double A_pq = gsl_matrix_get(A,p,q);
                double A_qq = gsl_matrix_get(A,q,q);
                double A_pp = gsl_matrix_get(A,p,p);

                double phi = 0.5*atan2(2*A_pq,(A_qq-A_pp));
                double s = sin(phi);
                double c = cos(phi);

                A_ij = c*c*A_pp - 2*c*s*A_pq + s*s*A_qq;
                gsl_matrix_set(A,p,p,A_ij);

                A_ij = s*s*A_pp + 2*c*s*A_pq + c*c*A_qq;
                gsl_matrix_set(A,q,q,A_ij);

                A_ij = s*c*(A_pp-A_qq) + (c*c-s*s)*A_pq;
                gsl_matrix_set(A,p,q,A_ij);
                gsl_matrix_set(A,q,p,A_ij);

                for (int i = 0; i < n; ++i) {
                    if (i != p && i != q){
                        double A_pi = gsl_matrix_get(A,p,i);
                        double A_qi = gsl_matrix_get(A,q,i);

                        A_ij = c*A_pi - s*A_qi;
                        gsl_matrix_set(A,p,i,A_ij);
                        gsl_matrix_set(A,i,p,A_ij);

                        A_ij = s*A_pi + c*A_qi;
                        gsl_matrix_set(A,q,i,A_ij);
                        gsl_matrix_set(A,i,q,A_ij);
                    }

                    double v_ip = gsl_matrix_get(V,i,p);
                    double v_iq = gsl_matrix_get(V,i,q);
                    gsl_matrix_set(V,i,p,c*v_ip - s*v_iq);
                    gsl_matrix_set(V,i,q,s*v_ip + c*v_iq);
                }

            }
        }
        
        //sweep ended

        // test if converged
        for (int p = 0; p < n; ++p) {
            for (int q = 0; q < n; ++q) {
                if (q != p){
                    sumOfOffElements += gsl_matrix_get(A,p,q);
                }
            }
        }
        if (abs(sumOfOffElements) > 1e-12){
//            cout << numberOfSweeps << endl;
//            printGslMatrix(A);
        }
        else{
            converged = true;
        }
    }

    cout << "Diagonalization of matrix ended" << endl;
    cout << "Number of sweeps used: " << numberOfSweeps << endl;
    cout << "Number of rotations used: " << numberOfRotations << endl;
//    printGslMatrix(V);

}

void jacobiEigByEigAlg(gsl_matrix * A, gsl_matrix * V, int numberOfEigVals){
    int n = A->size1;

    double sumOfOffElements;
    double numberOfSweeps;
    double numberOfRotations;

    for (int eigValNo = 0; eigValNo < numberOfEigVals; ++eigValNo) {
        bool converged = false;

        while (!converged){
            sumOfOffElements = 0;
            numberOfSweeps += 1;

            double A_ij;
            int p = eigValNo;

            for (int q = p+1; q < n; ++q) {
                numberOfRotations += 1;

                double A_pq = gsl_matrix_get(A,p,q);
                double A_qq = gsl_matrix_get(A,q,q);
                double A_pp = gsl_matrix_get(A,p,p);

                double phi = 0.5*atan2(2*A_pq,(A_qq-A_pp));
                double s = sin(phi);
                double c = cos(phi);

                A_ij = c*c*A_pp - 2*c*s*A_pq + s*s*A_qq;
                gsl_matrix_set(A,p,p,A_ij);

                A_ij = s*s*A_pp + 2*c*s*A_pq + c*c*A_qq;
                gsl_matrix_set(A,q,q,A_ij);

                A_ij = s*c*(A_pp-A_qq) + (c*c-s*s)*A_pq;
                gsl_matrix_set(A,p,q,A_ij);
                gsl_matrix_set(A,q,p,A_ij);

                for (int i = 0; i < n; ++i) {
                    if (i != p && i != q){
                        double A_pi = gsl_matrix_get(A,p,i);
                        double A_qi = gsl_matrix_get(A,q,i);

                        A_ij = c*A_pi - s*A_qi;
                        gsl_matrix_set(A,p,i,A_ij);
                        gsl_matrix_set(A,i,p,A_ij);

                        A_ij = s*A_pi + c*A_qi;
                        gsl_matrix_set(A,q,i,A_ij);
                        gsl_matrix_set(A,i,q,A_ij);
                    }

                    double v_ip = gsl_matrix_get(V,i,p);
                    double v_iq = gsl_matrix_get(V,i,q);
                    gsl_matrix_set(V,i,p,c*v_ip - s*v_iq);
                    gsl_matrix_set(V,i,q,s*v_ip + c*v_iq);
                }

            }


            //sweep ended

            // test if converged
            for (int q = 0; q < n; ++q) {
                if (q != p){
                    sumOfOffElements += gsl_matrix_get(A,p,q);
                }
            }
            if (abs(sumOfOffElements) > 1e-12){
//            cout << numberOfSweeps << endl;
//            printGslMatrix(A);
            }
            else{
                converged = true;
            }
        }
    }

    cout << "Diagonalization of matrix ended" << endl;
    cout << "Number of sweeps used: " << numberOfSweeps << endl;
    cout << "Number of rotations used: " << numberOfRotations << endl;
}

void jacobiClassical(gsl_matrix * A, gsl_matrix * V){
    // man bør kun arbejde med den strnegt øvre del af A og en vector med diagonalelementerne.
    // det er dog ok at gære som nedenstående. så længe der kun er n^2 operationer
    int n = A->size1;
    bool converged = false;

//    gsl_vector * diagonalElements = gsl_vector_alloc(n);
//    for (int i = 0; i < n; ++i) {
//        gsl_vector_set(diagonalElements,i,gsl_matrix_get(A,i,i));
//    }

    double sumOfOffElements;
    double numberOfSweeps;
    double numberOfRotations;
    gsl_vector * largestElements = gsl_vector_alloc(n-1);

    while (!converged){
        sumOfOffElements = 0;
        numberOfSweeps += 1;

        for (int i = 0; i < n-1; ++i) {
            double positionOfLargest = i+1;
            for (int j = i+1; j < n; ++j) {
//                cout << "i and j " << i << j << endl;
                double temp = abs(gsl_matrix_get(A,i,j));
                if (temp >= abs(gsl_matrix_get(A,i,positionOfLargest))) positionOfLargest = j;
            }
            gsl_vector_set(largestElements,i,positionOfLargest);
        }

//        printGslVector(largestElements);

        double A_ij;
        for (int p = 0; p < n-1; ++p) {
            numberOfRotations += 1;
            int q = gsl_vector_get(largestElements,p);

            double A_pq = gsl_matrix_get(A,p,q);
            double A_qq = gsl_matrix_get(A,q,q);
            double A_pp = gsl_matrix_get(A,p,p);

            double phi = 0.5*atan2(2*A_pq,(A_qq-A_pp));
            double s = sin(phi);
            double c = cos(phi);

            A_ij = c*c*A_pp - 2*c*s*A_pq + s*s*A_qq;
            gsl_matrix_set(A,p,p,A_ij);

            A_ij = s*s*A_pp + 2*c*s*A_pq + c*c*A_qq;
            gsl_matrix_set(A,q,q,A_ij);

            A_ij = s*c*(A_pp-A_qq) + (c*c-s*s)*A_pq;
            gsl_matrix_set(A,p,q,A_ij);
            gsl_matrix_set(A,q,p,A_ij);

//            cout << "3" << endl;

            for (int i = 0; i < n; ++i) {
                if (i != p && i != q){
                    double A_pi = gsl_matrix_get(A,p,i);
                    double A_qi = gsl_matrix_get(A,q,i);

                    A_ij = c*A_pi - s*A_qi;
                    gsl_matrix_set(A,p,i,A_ij);
                    gsl_matrix_set(A,i,p,A_ij);

                    A_ij = s*A_pi + c*A_qi;
                    gsl_matrix_set(A,q,i,A_ij);
                    gsl_matrix_set(A,i,q,A_ij);
                }

                double v_ip = gsl_matrix_get(V,i,p);
                double v_iq = gsl_matrix_get(V,i,q);
                gsl_matrix_set(V,i,p,c*v_ip - s*v_iq);
                gsl_matrix_set(V,i,q,s*v_ip + c*v_iq);
            }

        }

        //sweep ended
//        cout << "4" << endl;
        // test if converged
        for (int p = 0; p < n; ++p) {
            for (int q = 0; q < n; ++q) {
                if (q != p){
                    sumOfOffElements += gsl_matrix_get(A,p,q);
                }
            }
        }
        if (abs(sumOfOffElements) > 1e-12){
//            cout << numberOfSweeps << " " << sumOfOffElements << endl;
//            printGslMatrix(A);
        }
        else{
            converged = true;
        }
    }

    cout << "Diagonalization of matrix ended" << endl;
    cout << "Number of sweeps used: " << numberOfSweeps << endl;
    cout << "Number of rotations used: " << numberOfRotations << endl;
//    printGslMatrix(V);

}

gsl_matrix * createJacobiMatrix(int n, int p, int q, double phi){
    gsl_matrix * J = gsl_matrix_alloc(n,n);
    for (int i = 0; i < n; ++i) {
        gsl_matrix_set(J,i,i,1);
    }
    gsl_matrix_set(J,p,p,cos(phi));
    gsl_matrix_set(J,q,q,cos(phi));
    gsl_matrix_set(J,q,p,-sin(phi));
    gsl_matrix_set(J,p,q,sin(phi));

    return J;
}

double fRand(double fMin, double fMax){
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void printGslMatrix(gsl_matrix * A){
    int n = A->size1;
    int m = A->size2;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            double A_ij = gsl_matrix_get(A,i,j);
            if (abs(A_ij) < 1e-6){
                cout << 0 << "\t";
            } else{
                cout << setprecision(3) << A_ij << "\t";
            }
//            cout << setprecision(3) << A_ij << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void printGslVector(const gsl_vector * a){
    int n = a->size;
    cout << endl;
    for (int i = 0; i < n; ++i) {
        cout << setprecision(5) << gsl_vector_get(a,i) << endl;
    }
    cout << endl;
}